﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Total.ToolAcademy.Entity
{
    public class Order
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public virtual Guid Id { get; set; }

        public virtual DateTime Date { get; set; }

        public virtual Guid? UserId { get; set; }

        [DataType(DataType.Date)]
        public virtual DateTime? RequestedDeliveryDate { get; set; }

        [DataType(DataType.Date)]
        public virtual DateTime? EstimatedDeliveryDate { get; set; }

        public virtual string DispatchNote { get; set; }

        public virtual Guid? SignedOffById { get; set; }

        public virtual DateTime? SignedOffDate { get; set; }

        public virtual int? CourierId { get; set; }

        [StringLength(32)]
        public virtual string CourierTrackingReference { get; set; }

        public virtual OrderStatus Status { get; set; }

        [StringLength(1024)]
        public virtual string StatusMessage { get; set; }

        [Required]
        [StringLength(12)]
        public virtual string BillingTitle { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string BillingFirstName { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string BillingLastName { get; set; }

        [StringLength(64)]
        public virtual string BillingCompanyName { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string BillingBuildingAddress { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string BillingAddressLine1 { get; set; }

        [StringLength(64)]
        public virtual string BillingAddressLine2 { get; set; }

        [StringLength(64)]
        public virtual string BillingAddressLine3 { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string BillingTownCity { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string BillingCounty { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string BillingPostcode { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string BillingCountry { get; set; }

        [Required]
        [StringLength(16)]
        public virtual string BillingTelephone { get; set; }

        [StringLength(16)]
        public virtual string BillingMobile { get; set; }

        [Required]
        [StringLength(256)]
        public virtual string BillingEmail { get; set; }

        [Required]
        [StringLength(12)]
        public virtual string DeliveryTitle { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string DeliveryFirstName { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string DeliveryLastName { get; set; }

        [StringLength(64)]
        public virtual string DeliveryCompanyName { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string DeliveryBuildingAddress { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string DeliveryAddressLine1 { get; set; }

        [StringLength(64)]
        public virtual string DeliveryAddressLine2 { get; set; }

        [StringLength(64)]
        public virtual string DeliveryAddressLine3 { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string DeliveryTownCity { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string DeliveryCounty { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string DeliveryPostcode { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string DeliveryCountry { get; set; }

        [Required]
        [StringLength(16)]
        public virtual string DeliveryTelephone { get; set; }

        [StringLength(16)]
        public virtual string DeliveryMobile { get; set; }

        [Required]
        [StringLength(256)]
        public virtual string DeliveryEmail { get; set; }

        [StringLength(512)]
        public virtual string DeliveryInstructions { get; set; }

        public virtual User User { get; set; }

        public virtual User SignedOffBy { get; set; }

        public virtual Courier Courier { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; }

        public virtual Transaction Transaction { get; set; }

    }

    public enum OrderStatus
    {
        Pending, Paid, Dispatched
    }
}
