﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Total.ToolAcademy.Entity
{
    public class MailingList
    {
        public virtual int Id { get; set; }

        [Required]
        [StringLength(256)]
        public virtual string EmailAddress { get; set; }

        public virtual Guid? UserId { get; set; }

        public virtual User User { get; set; }
    }
}
