﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Total.ToolAcademy.Entity
{
    public class Page
    {
        public virtual int Id { get; set; }

        public virtual int? ParentPageId { get; set; }

        public virtual PageStatus Status { get; set; }

        [DataType(DataType.Date)]
        public virtual DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        public virtual DateTime? FinishDate { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string Name { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string Permalink { get; set; }

        [StringLength(512)]
        public virtual string Description { get; set; }

        public virtual string Content { get; set; }

        public virtual int SortOrder { get; set; }

        [StringLength(256)]
        public virtual string Image { get; set; }

        [StringLength(256)]
        public virtual string ImageAlt { get; set; }

        [StringLength(128)]
        public virtual string MetaTitle { get; set; }

        [StringLength(512)]
        public virtual string MetaDescription { get; set; }
        
        public virtual Guid CreatedById { get; set; }

        public virtual DateTime CreatedDateTime { get; set; }

        public virtual Guid? LastModifiedById { get; set; }

        public virtual DateTime? LastModifiedDateTime { get; set; }

        public virtual Guid? DeletedById { get; set; }

        public virtual DateTime? DeletedDateTime { get; set; }

        public virtual bool Deleted { get; set; }

        public virtual User CreatedBy { get; set; }

        public virtual User LastModifiedBy { get; set; }

        public virtual User DeletedBy { get; set; }

        public virtual Page ParentPage { get; set; }
    }

    public enum PageStatus
    {
        Live, TimeLimited, Hidden
    }
}
