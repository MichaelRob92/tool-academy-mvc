﻿namespace Total.ToolAcademy.Entity
{
    public class RelatedProduct
    {
        public virtual int Id { get; set; }

        public virtual RelatedProductType Type { get; set; }

        public virtual int PrimaryProductId { get; set; }

        public virtual int SecondaryProductId { get; set; }

        public virtual Product PrimaryProduct { get; set; }

        public virtual Product SecondaryProduct { get; set; }
    }

    public enum RelatedProductType
    {
        UpSell, CrossSell
    }
}
