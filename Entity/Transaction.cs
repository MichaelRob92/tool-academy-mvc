﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Total.ToolAcademy.Entity
{
    public class Transaction
    {
        public virtual int Id { get; set; }

        public virtual Guid OrderId { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string UniqueCode { get; set; }

        [Required]
        [StringLength(16)]
        public virtual string IpAddress { get; set; }

        [Required]
        [StringLength(32)]
        public virtual string SessionId { get; set; }

        public virtual DateTime TransactionDateTime { get; set; }

        [Required]
        public virtual string EncryptionString { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal NetAmount { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal TaxAmount { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal DeliveryAmount { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal GrossAmount { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal AuthorisedAmount { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal? DiscountAmount { get; set; }

        [Required]
        [StringLength(50)]
        public virtual string Currency { get; set; }

        public virtual bool Authorised { get; set; }

        [StringLength(128)]
        public virtual string AuthorisationCode { get; set; }

        public virtual DateTime? AuthorisedDateTime { get; set; }

        public virtual bool ManuallyAuthorised { get; set; }

        public virtual Guid? ManuallyAuthorisedById { get; set; }

        public virtual Order Order { get; set; }

        public virtual User ManuallyAuthorisedBy { get; set; }
    }
}
