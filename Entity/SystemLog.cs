﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Total.ToolAcademy.Entity
{
    public class SystemLog
    {
        public virtual int Id { get; set; }

        public virtual DateTime LogDateTime { get; set; }

        [Required]
        [StringLength(255)]
        public virtual string Thread { get; set; }

        [Required]
        [StringLength(50)]
        public virtual string Level { get; set; }

        [Required]
        [StringLength(255)]
        public virtual string Logger { get; set; }

        [Required]
        [StringLength(4000)]
        public virtual string Message { get; set; }

        [StringLength(2000)]
        public virtual string Exception { get; set; }
    }
}
