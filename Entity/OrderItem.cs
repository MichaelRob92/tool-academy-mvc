﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Total.ToolAcademy.Entity
{
    public class OrderItem
    {
        public virtual int Id { get; set; }

        public virtual Guid OrderId { get; set; }

        public virtual int ProductId { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string ProductName { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string ProductSummary { get; set; }

        public virtual string ProductVariation { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal NetAmount { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal TaxAmount { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal DeliveryAmount { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal? DiscountAmount { get; set; }

        public virtual int Quantity { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal GrossAmount { get; set; }

        public virtual Order Order { get; set; }

        public virtual Product Product { get; set; }
    }
}
