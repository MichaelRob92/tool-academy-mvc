﻿namespace Total.ToolAcademy.Entity
{
    public class CategoryProductAssociation
    {
        public virtual int Id { get; set; }

        public virtual int CategoryId { get; set; }

        public virtual int ProductId { get; set; }

        public virtual Category Category { get; set; }

        public virtual Product Product { get; set; }
    }
}
