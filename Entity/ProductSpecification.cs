﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Total.ToolAcademy.Entity
{
    public class ProductSpecification
    {
        public virtual int Id { get; set; }

        [ForeignKey("Product")]
        public virtual int ProductId { get; set; }

        [StringLength(256)]
        public virtual string Title { get; set; }

        [Required]
        [StringLength(512)]
        public virtual string Content { get; set; }

        public virtual int SortOrder { get; set; }

        [InverseProperty("ProductSpecifications")]
        public virtual Product Product { get; set; }
    }
}
