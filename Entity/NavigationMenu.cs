﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Total.ToolAcademy.Entity
{
    public class NavigationMenu
    {
        public virtual int Id { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string Name { get; set; }

        [StringLength(512)]
        public virtual string Link { get; set; }

        public virtual NavigationMenuLocation Location { get; set; }

        public virtual Guid CreatedById { get; set; }

        public virtual DateTime CreatedDateTime { get; set; }

        public virtual Guid? LastModifiedById { get; set; }

        public virtual DateTime? LastModifiedDateTime { get; set; }

        public virtual Guid? DeletedById { get; set; }

        public virtual DateTime? DeletedDateTime { get; set; }

        public virtual bool Deleted { get; set; }

        public virtual User CreatedBy { get; set; }

        public virtual User LastModifiedBy { get; set; }

        public virtual User DeletedBy { get; set; }

        public virtual ICollection<NavigationMenuLink> NavigationMenuLinks { get; set; }
    }

    public enum NavigationMenuLocation
    {
        MainMenu, FooterRow1Col2, FooterRow1Col3, FooterRow2Col2, FooterRow2Col3
    }
}