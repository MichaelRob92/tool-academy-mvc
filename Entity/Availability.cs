﻿using System.ComponentModel.DataAnnotations;

namespace Total.ToolAcademy.Entity
{
    public class Availability
    {
        public virtual int Id { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string Name { get; set; }

        [Required]
        public virtual AvailabilitySchema AvailabilitySchema { get; set; }
    }

    public enum AvailabilitySchema
    {
        Discontinued,
        InStock,
        InStoreOnly,
        LimitedAvailability,
        OnlineOnly,
        OutOfStock,
        PreOrder,
        SoldOut
    }
}
