﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Total.ToolAcademy.Entity
{
    public class ProductVariation
    {
        public virtual int Id { get; set; }

        public virtual int ProductId { get; set; }

        public virtual int ProductAttributeId { get; set; }

        public virtual int AttributeValueId { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal? PriceModifier { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal? DeliveryModifier { get; set; }

        public virtual decimal? WeightModifier { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal? VatModifier { get; set; }

        public virtual bool Active { get; set; }

        public virtual AttributeValue AttributeValue { get; set; }

        public virtual ProductAttribute ProductAttribute { get; set; }
        
        public virtual Product Product { get; set; }
    }
}
