﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Total.ToolAcademy.Entity
{
    public class Courier
    {
        public virtual int Id { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string Label { get; set; }

        [Required]
        [StringLength(256)]
        public virtual string Website { get; set; }

        public virtual int SortOrder { get; set; }

        public virtual Guid CreatedById { get; set; }

        public virtual DateTime CreatedDateTime { get; set; }

        public virtual Guid? LastModifiedById { get; set; }

        public virtual DateTime? LastModifiedDateTime { get; set; }

        public virtual Guid? DeletedById { get; set; }

        public virtual DateTime? DeletedDateTime { get; set; }

        public virtual bool Deleted { get; set; }

        public virtual User CreatedBy { get; set; }

        public virtual User LastModifiedBy { get; set; }

        public virtual User DeletedBy { get; set; }
    }
}
