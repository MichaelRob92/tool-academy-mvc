﻿namespace Total.ToolAcademy.Entity
{
    public class ProductAttribute
    {
        public virtual int Id { get; set; }

        public virtual int AttributeId { get; set; }

        public virtual bool Required { get; set; }

        public virtual Attribute Attribute { get; set; }
    }
}
