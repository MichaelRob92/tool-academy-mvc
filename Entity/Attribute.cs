﻿using System.ComponentModel.DataAnnotations;

namespace Total.ToolAcademy.Entity
{
    public class Attribute
    {
        public virtual int Id { get; set; }

        public virtual AttributeType Type { get; set; }

        public virtual AttributeStyle Style { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string Name { get; set; }
    }

    public enum AttributeType
    {
        Standard, BuyingOption
    }

    public enum AttributeStyle
    {
        List, SelectList, RadioList, CheckboxList, ImageGrid
    }
}
