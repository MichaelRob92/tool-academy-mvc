﻿using System.ComponentModel.DataAnnotations;

namespace Total.ToolAcademy.Entity
{
    public class NavigationMenuLink
    {
        public virtual int Id { get; set; }

        public virtual int NavigationMenuId { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string Name { get; set; }

        [Required]
        [StringLength(512)]
        public virtual string Link { get; set; }

        public virtual int SortOrder { get; set; }

        public virtual NavigationMenu NavigationMenu { get; set; }
    }
}
