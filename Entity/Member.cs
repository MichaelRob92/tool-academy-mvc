﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Total.ToolAcademy.Entity
{
    public class Member
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public virtual Guid UserId { get; set; }

        [Required]
        [StringLength(12)]
        public virtual string Title { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string FirstName { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string LastName { get; set; }

        [DataType(DataType.Date)]
        public virtual DateTime? DateOfBirth { get; set; }

        public virtual User User { get; set; }
    }
}
