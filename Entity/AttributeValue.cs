﻿using System.ComponentModel.DataAnnotations;

namespace Total.ToolAcademy.Entity
{
    public class AttributeValue
    {
        public virtual int Id { get; set; }

        public virtual int AttributeId { get; set; }

        [Required]
        [StringLength(256)]
        public virtual string Value { get; set; }

        [StringLength(256)]
        public virtual string Image { get; set; }

        [StringLength(256)]
        public virtual string ImageAlt { get; set; }

        public virtual int SortOrder { get; set; }

        public virtual Attribute Attribute { get; set; }
    }
}
