﻿using System.ComponentModel.DataAnnotations;

namespace Total.ToolAcademy.Entity
{
    public class ProductMedia
    {
        public virtual int Id { get; set; }

        public virtual int ProductId { get; set; }

        public virtual ProductMediaType Type { get; set; }

        [Required]
        [StringLength(256)]
        public virtual string Value { get; set; }

        public virtual int SortOrder { get; set; }

        public virtual Product Product { get; set; }
    }

    public enum ProductMediaType
    {
        Image, Video
    }
}
