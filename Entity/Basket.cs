﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Total.ToolAcademy.Entity
{
    public class Basket
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public virtual Guid Id { get; set; }

        public virtual DateTime Date { get; set; }

        public virtual Guid? UserId { get; set; }

        [Required]
        [StringLength(16)]
        public virtual string IpAddress { get; set; }

        [Required]
        [StringLength(32)]
        public virtual string SessionId { get; set; }

        public virtual ICollection<BasketProduct> BasketProducts { get; set; }
    }
}
