﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Total.ToolAcademy.Entity
{
    public class BasketProduct
    {
        [Key]
        public virtual int Id { get; set; }

        public virtual Guid BasketId { get; set; }

        public virtual int ProductId { get; set; }

        public virtual int Quantity { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal Price { get; set; }

        public virtual Basket Basket { get; set; }

        public virtual Product Product { get; set; }

        public virtual ICollection<BasketProductVariation> BasketProductVariations { get; set; }
    }
}
