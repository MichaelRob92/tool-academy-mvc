﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Total.ToolAcademy.Entity
{
    public class User : IdentityUser<Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>
    {
        public User()
        {
            Id = Guid.NewGuid();
        }

        public User(string name) : this()
        {
            UserName = name;
        }

        public virtual Member Member { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User, Guid> manager)
        {
            var identity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return identity;
        }
    }
    
    public class GuidUserLogin : IdentityUserLogin<Guid>
    {
        
    }

    public class GuidUserRole : IdentityUserRole<Guid>
    {

    }

    public class GuidUserClaim : IdentityUserClaim<Guid>
    {
        
    }
}
