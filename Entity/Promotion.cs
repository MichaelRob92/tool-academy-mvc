﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Total.ToolAcademy.Entity
{
    public class Promotion
    {
        public virtual int Id { get; set; }

        public virtual PromotionType Type { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string Name { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string Code { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal? DiscountValue { get; set; }

        public virtual decimal? DiscountPercent { get; set; }

        [DataType(DataType.Date)]
        public virtual DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        public virtual DateTime? FinishDate { get; set; }

        public virtual int? Usages { get; set; }

        public virtual Guid? UserId { get; set; }

        [StringLength(256)]
        public virtual string Email { get; set; }

        public virtual Guid CreatedById { get; set; }

        public virtual DateTime CreatedDateTime { get; set; }

        public virtual Guid? LastModifiedById { get; set; }

        public virtual DateTime? LastModifiedDateTime { get; set; }

        public virtual Guid? DeletedById { get; set; }

        public virtual DateTime? DeletedDateTime { get; set; }

        public virtual bool Deleted { get; set; }

        public virtual User CreatedBy { get; set; }

        public virtual User LastModifiedBy { get; set; }

        public virtual User DeletedBy { get; set; }
    }

    public enum PromotionType
    {
        TimeLimited, Usages, Unique, Email
    }
}
