﻿using System.ComponentModel.DataAnnotations;

namespace Total.ToolAcademy.Entity
{
    public class BasketProductVariation
    {
        [Key]
        public virtual int Id { get; set; }

        public virtual int BasketProductId { get; set; }

        public virtual int ProductVariationId { get; set; }

        public virtual BasketProduct BasketProduct { get; set; }

        public virtual ProductVariation ProductVariation { get; set; }
    }
}
