﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Total.ToolAcademy.Entity
{
    public class SystemMessage
    {
        public virtual int Id { get; set; }

        public virtual DateTime LogDateTime { get; set; }

        [Required]
        [StringLength(4000)]
        public virtual string Message { get; set; }
    }
}
