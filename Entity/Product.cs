﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Total.ToolAcademy.Entity
{
    public class Product
    {
        public virtual int Id { get; set; }

        public virtual int BrandId { get; set; }

        public virtual ProductStatus Status { get; set; }

        [DataType(DataType.Date)]
        public virtual DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        public virtual DateTime? FinishDate { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string Name { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string Permalink { get; set; }
        
        [StringLength(32)]
        public virtual string Code { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string Summary { get; set; }

        [Required]
        public virtual string Content { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal Price { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public virtual decimal? OfferPrice { get; set; }

        public virtual bool VatExempt { get; set; }

        public virtual decimal? VatRate { get; set; }

        [StringLength(256)]
        public virtual string Image { get; set; }

        [StringLength(256)]
        public virtual string ImageAlt { get; set; }

        public virtual bool SpecialOffer { get; set; }

        [StringLength(512)]
        public virtual string SpecialOfferMessage { get; set; }

        public virtual decimal Weight { get; set; }

        public virtual int SortOrder { get; set; }

        public virtual bool Discontinued { get; set; }

        [StringLength(512)]
        public virtual string DiscontinuedMessage { get; set; }

        public virtual bool Featured { get; set; }

        [StringLength(128)]
        public virtual string MetaTitle { get; set; }

        [StringLength(512)]
        public virtual string MetaDescription { get; set; }

        public virtual int AvailabilityId { get; set; }

        public virtual Guid CreatedById { get; set; }

        public virtual DateTime CreatedDateTime { get; set; }

        public virtual Guid? LastModifiedById { get; set; }

        public virtual DateTime? LastModifiedDateTime { get; set; }

        public virtual Guid? DeletedById { get; set; }

        public virtual DateTime? DeletedDateTime { get; set; }

        public virtual bool Deleted { get; set; }

        public virtual Brand Brand { get; set; }

        public virtual Availability Availability { get; set; }

        public virtual User CreatedBy { get; set; }

        public virtual User LastModifiedBy { get; set; }

        public virtual User DeletedBy { get; set; }

        public virtual ICollection<ProductMedia> ProductMedia { get; set; } 

        public virtual ICollection<ProductVariation> ProductVariations { get; set; }

        [InverseProperty("Product")]
        public virtual ICollection<ProductSpecification> ProductSpecifications { get; set; } 

        public virtual ICollection<CategoryProductAssociation> CategoryProductAssociations { get; set; }

        public virtual ICollection<RelatedProduct> RelatedProducts { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; } 
    }

    public enum ProductStatus
    {
        Live, TimeLimited, Hidden
    }
}
