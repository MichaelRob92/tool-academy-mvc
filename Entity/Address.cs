﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Total.ToolAcademy.Entity
{
    public class Address
    {
        public virtual int Id { get; set; }

        [Required]
        public virtual Guid UserId { get; set; }

        [Required]
        [StringLength(128)]
        public virtual string Label { get; set; }

        [Required]
        [StringLength(12)]
        public virtual string Title { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string FirstName { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string LastName { get; set; }

        [StringLength(64)]
        public virtual string CompanyName { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string BuildingAddress { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string AddressLine1 { get; set; }

        [StringLength(64)]
        public virtual string AddressLine2 { get; set; }

        [StringLength(64)]
        public virtual string AddressLine3 { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string TownCity { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string County { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string Postcode { get; set; }

        [Required]
        [StringLength(64)]
        public virtual string Country { get; set; }

        [Required]
        [StringLength(16)]
        public virtual string Telephone { get; set; }

        [StringLength(16)]
        public virtual string Mobile { get; set; }

        [Required]
        [StringLength(256)]
        public virtual string Email { get; set; }

        [StringLength(512)]
        public virtual string Instructions { get; set; }

        public virtual bool Default { get; set; }

        public virtual User User { get; set; }
    }
}
