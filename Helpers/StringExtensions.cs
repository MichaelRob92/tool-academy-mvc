﻿using System.Net.Mail;

namespace Total.ToolAcademy.Helpers
{
    public static class StringExtensions
    {

        public static bool IsValidEmail(this string address)
        {
            try
            {
                var mailAddress = new MailAddress(address);
                return mailAddress.Address.Equals(address);
            }
            catch
            {
                return false;
            }
        }
    }
}
