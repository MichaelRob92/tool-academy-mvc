﻿using System;
using Total.ToolAcademy.Entity;

namespace Total.ToolAcademy.Helpers
{
    public static class BrandExtensions
    {
        public static bool IsValid(this Brand brand)
        {
            if (brand.Deleted)
                return false;

            if (brand.Status == BrandStatus.Hidden)
                return false;

            if (brand.Status == BrandStatus.Live)
                return true;

            var currentDate = DateTime.Now;
            return !(brand.StartDate < currentDate) && !(brand.FinishDate > currentDate);
        }
    }
}
