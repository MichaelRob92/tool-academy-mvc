﻿using System;
using Total.ToolAcademy.Entity;

namespace Total.ToolAcademy.Helpers
{
    public static class ProductExtensions
    {
        public static decimal GetPrice(this Product product)
        {
            return product.OfferPrice ?? product.Price;
        }

        public static decimal CalculateVatPrice(this decimal price, bool productVatExempt, decimal? productVatRate, decimal vatRate = 20.0m)
        {
            if (productVatExempt)
                return price;

            if (productVatRate.HasValue)
                vatRate = productVatRate.Value;
            var vatValue = vatRate*(price/100);
            return price + vatValue;
        }

        public static bool IsValid(this Product product)
        {
            if (product.Deleted)
                return false;

            if (product.Status == ProductStatus.Hidden)
                return false;

            if (product.Status == ProductStatus.Live)
                return true;

            var currentDate = DateTime.Now;
            return !(product.StartDate < currentDate) && !(product.FinishDate > currentDate);
        }
    }
}
