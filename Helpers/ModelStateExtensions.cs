﻿using System.Linq;
using System.Web.Mvc;

namespace Total.ToolAcademy.Helpers
{
    public static class ModelStateExtensions
    {
        public static bool HasError(this ModelStateDictionary dictionary, string key)
        {
            return dictionary.ContainsKey(key) && dictionary[key].Errors.Any();
        }
    }
}