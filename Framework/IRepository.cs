﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Total.ToolAcademy.Framework
{
    public interface IRepository<T> : IDisposable where T : class
    {
        void Attach(T entity);

        void Add(params T[] entities);

        T First(Expression<Func<T, bool>> predicate);

        T FirstOrDefault(Expression<Func<T, bool>> predicate);

        Task<T> FirstAsync(Expression<Func<T, bool>> predicate);

        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate);

        Task<int> CountAsync();

        Task<int> CountAsync(Expression<Func<T, bool>> predicate);

        Task<bool> AnyAsync(Expression<Func<T, bool>> predicate);

        IQueryable<T> Where(Expression<Func<T, bool>> predicate);

        IQueryable<T> Where<TProperty>(Expression<Func<T, bool>> predicate, Expression<Func<T, TProperty>> order);

        IQueryable<T> Where(Expression<Func<T, bool>> predicate, int count);

        IQueryable<T> Where<TProperty>(Expression<Func<T, bool>> predicate, Expression<Func<T, TProperty>> order, int count);

        IQueryable<TResult> Select<TResult>(Expression<Func<T, TResult>> selector);

        IQueryable<TResult> Select<TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector);

        IQueryable<TResult> Select<TResult, TOrder>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector, Expression<Func<T, TOrder>> order);

        IQueryable<T> Include(params string[] includes);

        IQueryable<T> Include<TProperty>(params Expression<Func<T, TProperty>>[] includeExpressions);
    }
}
