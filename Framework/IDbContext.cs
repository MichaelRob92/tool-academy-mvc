﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Total.ToolAcademy.Entity;

namespace Total.ToolAcademy.Framework
{
    public interface IDbContext : IDisposable
    {
        DbContext DbContext { get; }
        IDbSet<TEntity> Set<TEntity>() where TEntity : class;
        Task<int> SaveChangesAsync();
        IDbSet<Address> Addresses { get; set; }
        IDbSet<Entity.Attribute> Attributes { get; set; }
        IDbSet<AttributeValue> AttributeValues { get; set; }
        IDbSet<Availability> Availabilities { get; set; } 
        IDbSet<Banner> Banners { get; set; }
        IDbSet<Basket> Baskets { get; set; }
        IDbSet<BasketProduct> BasketProducts { get; set; }
        IDbSet<Brand> Brands { get; set; }
        IDbSet<Category> Categories { get; set; }
        IDbSet<CategoryProductAssociation> CategoryProductAssociations { get; set; }
        IDbSet<Courier> Couriers { get; set; }
        IDbSet<MailingList> MailingLists { get; set; }
        IDbSet<Member> Members { get; set; }
        IDbSet<NavigationMenu> NavigationMenus { get; set; }
        IDbSet<NavigationMenuLink> NavigationMenuLinks { get; set; }
        IDbSet<Order> Orders { get; set; }
        IDbSet<OrderItem> OrderItems { get; set; }
        IDbSet<Page> Pages { get; set; }
        IDbSet<Product> Products { get; set; }
        IDbSet<ProductAttribute> ProductAttributes { get; set; }
        IDbSet<ProductMedia> ProductMedia { get; set; }
        IDbSet<ProductVariation> ProductVariations { get; set; }
        IDbSet<ProductSpecification> ProductSpecifications { get; set; }
        IDbSet<Promotion> Promotions { get; set; }
        IDbSet<RelatedProduct> RelatedProducts { get; set; }
        IDbSet<Transaction> Transactions { get; set; }
        IDbSet<GuidUserRole> UserRoles { get; set; }
        IDbSet<SystemLog> SystemLogs { get; set; } 
    }
}
