﻿using System;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Total.ToolAcademy.Entity;

namespace Total.ToolAcademy.Context.Identity
{
    public class ToolAcademyUserStore : UserStore<User, Role, Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>
    {
        public ToolAcademyUserStore(DbContext context) : base(context)
        {
        }
    }
}
