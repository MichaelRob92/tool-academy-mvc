﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using Total.ToolAcademy.Entity;
using Total.ToolAcademy.Framework;

namespace Total.ToolAcademy.Context.Identity
{
    public class ToolAcademyUserManager : UserManager<User, Guid>
    {
        public ToolAcademyUserManager(IUserStore<User, Guid> store) : base(store)
        {
        }

        public static ToolAcademyUserManager Create(IDataProtectionProvider protectionProvider, IDbContext context)
        { 
            var manager = new ToolAcademyUserManager(new ToolAcademyUserStore(context.DbContext));
            manager.UserValidator = new UserValidator<User, Guid>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false
            };
            if (protectionProvider != null)
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<User, Guid>(protectionProvider.Create("ASP.NET Identity"))
                    {
                        TokenLifespan = TimeSpan.FromHours(3)
                    };
            return manager;
        }
    }
}
