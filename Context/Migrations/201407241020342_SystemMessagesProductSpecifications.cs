namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SystemMessagesProductSpecifications : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductSpecification",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        Title = c.String(maxLength: 256),
                        Content = c.String(nullable: false, maxLength: 512),
                        SortOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Product", t => t.ProductId, cascadeDelete: false)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.SystemMessage",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LogDateTime = c.DateTime(nullable: false),
                        Message = c.String(nullable: false, maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductSpecification", "ProductId", "dbo.Product");
            DropIndex("dbo.ProductSpecification", new[] { "ProductId" });
            DropTable("dbo.SystemMessage");
            DropTable("dbo.ProductSpecification");
        }
    }
}
