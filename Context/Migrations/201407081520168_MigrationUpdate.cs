namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrationUpdate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductVariation", "AttributeValueId", "dbo.AttributeValue");
            AddForeignKey("dbo.ProductVariation", "AttributeValueId", "dbo.AttributeValue", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductVariation", "AttributeValueId", "dbo.AttributeValue");
            AddForeignKey("dbo.ProductVariation", "AttributeValueId", "dbo.AttributeValue", "Id", cascadeDelete: true);
        }
    }
}
