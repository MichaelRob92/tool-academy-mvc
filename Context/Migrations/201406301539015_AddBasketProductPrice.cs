namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBasketProductPrice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BasketProduct", "Price", c => c.Decimal(nullable: false, storeType: "money"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BasketProduct", "Price");
        }
    }
}
