namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NavigationMenus : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NavigationMenuLink",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NavigationMenuId = c.Int(nullable: false),
                        Link = c.String(nullable: false, maxLength: 512),
                        SortOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NavigationMenu", t => t.NavigationMenuId, cascadeDelete: true)
                .Index(t => t.NavigationMenuId);
            
            CreateTable(
                "dbo.NavigationMenu",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Location = c.Int(nullable: false),
                        CreatedById = c.Guid(nullable: false),
                        CreatedDateTime = c.DateTime(nullable: false),
                        LastModifiedById = c.Guid(),
                        LastModifiedDateTime = c.DateTime(),
                        DeletedById = c.Guid(),
                        DeletedDateTime = c.DateTime(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreatedById, cascadeDelete: false)
                .ForeignKey("dbo.User", t => t.DeletedById)
                .ForeignKey("dbo.User", t => t.LastModifiedById)
                .Index(t => t.CreatedById)
                .Index(t => t.LastModifiedById)
                .Index(t => t.DeletedById);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NavigationMenuLink", "NavigationMenuId", "dbo.NavigationMenu");
            DropForeignKey("dbo.NavigationMenu", "LastModifiedById", "dbo.User");
            DropForeignKey("dbo.NavigationMenu", "DeletedById", "dbo.User");
            DropForeignKey("dbo.NavigationMenu", "CreatedById", "dbo.User");
            DropIndex("dbo.NavigationMenu", new[] { "DeletedById" });
            DropIndex("dbo.NavigationMenu", new[] { "LastModifiedById" });
            DropIndex("dbo.NavigationMenu", new[] { "CreatedById" });
            DropIndex("dbo.NavigationMenuLink", new[] { "NavigationMenuId" });
            DropTable("dbo.NavigationMenu");
            DropTable("dbo.NavigationMenuLink");
        }
    }
}
