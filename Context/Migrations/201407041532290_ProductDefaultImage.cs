namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductDefaultImage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Product", "Image", c => c.String(maxLength: 256));
            AddColumn("dbo.Product", "ImageAlt", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Product", "ImageAlt");
            DropColumn("dbo.Product", "Image");
        }
    }
}
