namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateBasketProduct : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BasketProductVariation", "ProductVariation_Id", "dbo.ProductVariation");
            DropIndex("dbo.BasketProductVariation", new[] { "ProductVariation_Id" });
            RenameColumn(table: "dbo.BasketProductVariation", name: "ProductVariation_Id", newName: "ProductVariationId");
            AlterColumn("dbo.BasketProductVariation", "ProductVariationId", c => c.Int(nullable: false));
            CreateIndex("dbo.BasketProductVariation", "ProductVariationId");
            AddForeignKey("dbo.BasketProductVariation", "ProductVariationId", "dbo.ProductVariation", "Id", cascadeDelete: false);
            DropColumn("dbo.BasketProductVariation", "PublicVariationId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BasketProductVariation", "PublicVariationId", c => c.Int(nullable: false));
            DropForeignKey("dbo.BasketProductVariation", "ProductVariationId", "dbo.ProductVariation");
            DropIndex("dbo.BasketProductVariation", new[] { "ProductVariationId" });
            AlterColumn("dbo.BasketProductVariation", "ProductVariationId", c => c.Int());
            RenameColumn(table: "dbo.BasketProductVariation", name: "ProductVariationId", newName: "ProductVariation_Id");
            CreateIndex("dbo.BasketProductVariation", "ProductVariation_Id");
            AddForeignKey("dbo.BasketProductVariation", "ProductVariation_Id", "dbo.ProductVariation", "Id");
        }
    }
}
