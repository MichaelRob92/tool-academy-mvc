namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NavigationMenuLinkLink : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NavigationMenu", "Link", c => c.String(maxLength: 512));
        }
        
        public override void Down()
        {
            DropColumn("dbo.NavigationMenu", "Link");
        }
    }
}
