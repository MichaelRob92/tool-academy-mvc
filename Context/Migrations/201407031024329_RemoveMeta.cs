namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveMeta : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MemberMeta", "MemberUserId", "dbo.Member");
            DropForeignKey("dbo.BrandMeta", "BrandId", "dbo.Brand");
            DropForeignKey("dbo.CategoryMeta", "CategoryId", "dbo.Category");
            DropForeignKey("dbo.ProductMeta", "ProductId", "dbo.Product");
            DropForeignKey("dbo.PageMeta", "PageId", "dbo.Page");
            DropIndex("dbo.MemberMeta", new[] { "MemberUserId" });
            DropIndex("dbo.BrandMeta", new[] { "BrandId" });
            DropIndex("dbo.CategoryMeta", new[] { "CategoryId" });
            DropIndex("dbo.ProductMeta", new[] { "ProductId" });
            DropIndex("dbo.PageMeta", new[] { "PageId" });
            AddColumn("dbo.Product", "Permalink", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Product", "MetaTitle", c => c.String(maxLength: 128));
            AddColumn("dbo.Product", "MetaDescription", c => c.String(maxLength: 512));
            AddColumn("dbo.Brand", "Permalink", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Brand", "MetaTitle", c => c.String(maxLength: 128));
            AddColumn("dbo.Brand", "MetaDescription", c => c.String(maxLength: 512));
            AddColumn("dbo.Category", "Permalink", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Category", "MetaTitle", c => c.String(maxLength: 128));
            AddColumn("dbo.Category", "MetaDescription", c => c.String(maxLength: 512));
            AddColumn("dbo.Page", "Permalink", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Page", "MetaTitle", c => c.String(maxLength: 128));
            AddColumn("dbo.Page", "MetaDescription", c => c.String(maxLength: 512));
            DropTable("dbo.MemberMeta");
            DropTable("dbo.BrandMeta");
            DropTable("dbo.CategoryMeta");
            DropTable("dbo.ProductMeta");
            DropTable("dbo.PageMeta");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PageMeta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PageId = c.Int(nullable: false),
                        Key = c.Int(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductMeta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        Key = c.Int(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CategoryMeta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        Key = c.Int(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BrandMeta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BrandId = c.Int(nullable: false),
                        Key = c.Int(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MemberMeta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MemberUserId = c.Guid(nullable: false),
                        Key = c.Int(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.Page", "MetaDescription");
            DropColumn("dbo.Page", "MetaTitle");
            DropColumn("dbo.Page", "Permalink");
            DropColumn("dbo.Category", "MetaDescription");
            DropColumn("dbo.Category", "MetaTitle");
            DropColumn("dbo.Category", "Permalink");
            DropColumn("dbo.Brand", "MetaDescription");
            DropColumn("dbo.Brand", "MetaTitle");
            DropColumn("dbo.Brand", "Permalink");
            DropColumn("dbo.Product", "MetaDescription");
            DropColumn("dbo.Product", "MetaTitle");
            DropColumn("dbo.Product", "Permalink");
            CreateIndex("dbo.PageMeta", "PageId");
            CreateIndex("dbo.ProductMeta", "ProductId");
            CreateIndex("dbo.CategoryMeta", "CategoryId");
            CreateIndex("dbo.BrandMeta", "BrandId");
            CreateIndex("dbo.MemberMeta", "MemberUserId");
            AddForeignKey("dbo.PageMeta", "PageId", "dbo.Page", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ProductMeta", "ProductId", "dbo.Product", "Id", cascadeDelete: true);
            AddForeignKey("dbo.CategoryMeta", "CategoryId", "dbo.Category", "Id", cascadeDelete: true);
            AddForeignKey("dbo.BrandMeta", "BrandId", "dbo.Brand", "Id", cascadeDelete: true);
            AddForeignKey("dbo.MemberMeta", "MemberUserId", "dbo.Member", "UserId", cascadeDelete: true);
        }
    }
}
