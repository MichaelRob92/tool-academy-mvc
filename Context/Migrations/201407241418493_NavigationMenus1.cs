namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NavigationMenus1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.NavigationMenu", "CreatedById", "dbo.User");
            AddForeignKey("dbo.NavigationMenu", "CreatedById", "dbo.User", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NavigationMenu", "CreatedById", "dbo.User");
            AddForeignKey("dbo.NavigationMenu", "CreatedById", "dbo.User", "Id", cascadeDelete: true);
        }
    }
}
