namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductSpecificationForeignKey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductSpecification", "Product_Id", "dbo.Product");
            DropForeignKey("dbo.ProductSpecification", "ProductId", "dbo.Product");
            DropIndex("dbo.ProductSpecification", new[] { "ProductId" });
            DropIndex("dbo.ProductSpecification", new[] { "Product_Id" });
            DropColumn("dbo.ProductSpecification", "ProductId");
            RenameColumn(table: "dbo.ProductSpecification", name: "Product_Id", newName: "ProductId");
            AlterColumn("dbo.ProductSpecification", "ProductId", c => c.Int(nullable: false));
            CreateIndex("dbo.ProductSpecification", "ProductId");
            AddForeignKey("dbo.ProductSpecification", "ProductId", "dbo.Product", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductSpecification", "ProductId", "dbo.Product");
            DropIndex("dbo.ProductSpecification", new[] { "ProductId" });
            AlterColumn("dbo.ProductSpecification", "ProductId", c => c.Int());
            RenameColumn(table: "dbo.ProductSpecification", name: "ProductId", newName: "Product_Id");
            AddColumn("dbo.ProductSpecification", "ProductId", c => c.Int(nullable: false));
            CreateIndex("dbo.ProductSpecification", "Product_Id");
            CreateIndex("dbo.ProductSpecification", "ProductId");
            AddForeignKey("dbo.ProductSpecification", "ProductId", "dbo.Product", "Id");
            AddForeignKey("dbo.ProductSpecification", "Product_Id", "dbo.Product", "Id");
        }
    }
}
