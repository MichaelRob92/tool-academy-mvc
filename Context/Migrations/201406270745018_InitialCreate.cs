namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.UserRole",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        Label = c.String(nullable: false, maxLength: 128),
                        Title = c.String(nullable: false, maxLength: 12),
                        FirstName = c.String(nullable: false, maxLength: 64),
                        LastName = c.String(nullable: false, maxLength: 64),
                        CompanyName = c.String(maxLength: 64),
                        BuildingAddress = c.String(nullable: false, maxLength: 64),
                        AddressLine1 = c.String(nullable: false, maxLength: 64),
                        AddressLine2 = c.String(maxLength: 64),
                        AddressLine3 = c.String(maxLength: 64),
                        TownCity = c.String(nullable: false, maxLength: 64),
                        County = c.String(nullable: false, maxLength: 64),
                        Postcode = c.String(nullable: false, maxLength: 64),
                        Country = c.String(nullable: false, maxLength: 64),
                        Telephone = c.String(nullable: false, maxLength: 16),
                        Mobile = c.String(maxLength: 16),
                        Email = c.String(nullable: false, maxLength: 256),
                        Instructions = c.String(maxLength: 512),
                        Default = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserLogin",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Member",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        Title = c.String(nullable: false, maxLength: 12),
                        FirstName = c.String(nullable: false, maxLength: 64),
                        LastName = c.String(nullable: false, maxLength: 64),
                        DateOfBirth = c.DateTime(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.MemberMeta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MemberUserId = c.Guid(nullable: false),
                        Key = c.Int(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Member", t => t.MemberUserId, cascadeDelete: true)
                .Index(t => t.MemberUserId);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Date = c.DateTime(nullable: false),
                        UserId = c.Guid(),
                        RequestedDeliveryDate = c.DateTime(),
                        EstimatedDeliveryDate = c.DateTime(),
                        DispatchNote = c.String(),
                        SignedOffById = c.Guid(),
                        SignedOffDate = c.DateTime(),
                        CourierId = c.Int(),
                        CourierTrackingReference = c.String(maxLength: 32),
                        Status = c.Int(nullable: false),
                        StatusMessage = c.String(maxLength: 1024),
                        BillingTitle = c.String(nullable: false, maxLength: 12),
                        BillingFirstName = c.String(nullable: false, maxLength: 64),
                        BillingLastName = c.String(nullable: false, maxLength: 64),
                        BillingCompanyName = c.String(maxLength: 64),
                        BillingBuildingAddress = c.String(nullable: false, maxLength: 64),
                        BillingAddressLine1 = c.String(nullable: false, maxLength: 64),
                        BillingAddressLine2 = c.String(maxLength: 64),
                        BillingAddressLine3 = c.String(maxLength: 64),
                        BillingTownCity = c.String(nullable: false, maxLength: 64),
                        BillingCounty = c.String(nullable: false, maxLength: 64),
                        BillingPostcode = c.String(nullable: false, maxLength: 64),
                        BillingCountry = c.String(nullable: false, maxLength: 64),
                        BillingTelephone = c.String(nullable: false, maxLength: 16),
                        BillingMobile = c.String(maxLength: 16),
                        BillingEmail = c.String(nullable: false, maxLength: 256),
                        DeliveryTitle = c.String(nullable: false, maxLength: 12),
                        DeliveryFirstName = c.String(nullable: false, maxLength: 64),
                        DeliveryLastName = c.String(nullable: false, maxLength: 64),
                        DeliveryCompanyName = c.String(maxLength: 64),
                        DeliveryBuildingAddress = c.String(nullable: false, maxLength: 64),
                        DeliveryAddressLine1 = c.String(nullable: false, maxLength: 64),
                        DeliveryAddressLine2 = c.String(maxLength: 64),
                        DeliveryAddressLine3 = c.String(maxLength: 64),
                        DeliveryTownCity = c.String(nullable: false, maxLength: 64),
                        DeliveryCounty = c.String(nullable: false, maxLength: 64),
                        DeliveryPostcode = c.String(nullable: false, maxLength: 64),
                        DeliveryCountry = c.String(nullable: false, maxLength: 64),
                        DeliveryTelephone = c.String(nullable: false, maxLength: 16),
                        DeliveryMobile = c.String(maxLength: 16),
                        DeliveryEmail = c.String(nullable: false, maxLength: 256),
                        DeliveryInstructions = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courier", t => t.CourierId)
                .ForeignKey("dbo.User", t => t.SignedOffById)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.SignedOffById)
                .Index(t => t.CourierId);
            
            CreateTable(
                "dbo.Courier",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Label = c.String(nullable: false, maxLength: 128),
                        Website = c.String(nullable: false, maxLength: 256),
                        SortOrder = c.Int(nullable: false),
                        CreatedById = c.Guid(nullable: false),
                        CreatedDateTime = c.DateTime(nullable: false),
                        LastModifiedById = c.Guid(),
                        LastModifiedDateTime = c.DateTime(),
                        DeletedById = c.Guid(),
                        DeletedDateTime = c.DateTime(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreatedById)
                .ForeignKey("dbo.User", t => t.DeletedById)
                .ForeignKey("dbo.User", t => t.LastModifiedById)
                .Index(t => t.CreatedById)
                .Index(t => t.LastModifiedById)
                .Index(t => t.DeletedById);
            
            CreateTable(
                "dbo.OrderItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.Guid(nullable: false),
                        ProductId = c.Int(nullable: false),
                        ProductName = c.String(nullable: false, maxLength: 128),
                        ProductSummary = c.String(nullable: false, maxLength: 128),
                        ProductVariation = c.String(),
                        NetAmount = c.Decimal(nullable: false, storeType: "money"),
                        TaxAmount = c.Decimal(nullable: false, storeType: "money"),
                        DeliveryAmount = c.Decimal(nullable: false, storeType: "money"),
                        DiscountAmount = c.Decimal(storeType: "money"),
                        Quantity = c.Int(nullable: false),
                        GrossAmount = c.Decimal(nullable: false, storeType: "money"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Order", t => t.OrderId, cascadeDelete: true)
                .ForeignKey("dbo.Product", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.OrderId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BrandId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(),
                        FinishDate = c.DateTime(),
                        Name = c.String(nullable: false, maxLength: 128),
                        Code = c.String(maxLength: 32),
                        Summary = c.String(nullable: false, maxLength: 128),
                        Content = c.String(nullable: false),
                        Price = c.Decimal(nullable: false, storeType: "money"),
                        OfferPrice = c.Decimal(storeType: "money"),
                        SpecialOffer = c.Boolean(nullable: false),
                        SpecialOfferMessage = c.String(maxLength: 512),
                        Weight = c.Decimal(precision: 18, scale: 2),
                        SortOrder = c.Int(nullable: false),
                        Discontinued = c.Boolean(nullable: false),
                        DiscontinuedMessage = c.String(maxLength: 512),
                        Featured = c.Boolean(nullable: false),
                        CreatedById = c.Guid(nullable: false),
                        CreatedDateTime = c.DateTime(nullable: false),
                        LastModifiedById = c.Guid(),
                        LastModifiedDateTime = c.DateTime(),
                        DeletedById = c.Guid(),
                        DeletedDateTime = c.DateTime(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Brand", t => t.BrandId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.CreatedById)
                .ForeignKey("dbo.User", t => t.DeletedById)
                .ForeignKey("dbo.User", t => t.LastModifiedById)
                .Index(t => t.BrandId)
                .Index(t => t.CreatedById)
                .Index(t => t.LastModifiedById)
                .Index(t => t.DeletedById);
            
            CreateTable(
                "dbo.Brand",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(),
                        FinishDate = c.DateTime(),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(maxLength: 512),
                        Content = c.String(),
                        SortOrder = c.Int(nullable: false),
                        Image = c.String(maxLength: 256),
                        ImageAlt = c.String(maxLength: 256),
                        CreatedById = c.Guid(nullable: false),
                        CreatedDateTime = c.DateTime(nullable: false),
                        LastModifiedById = c.Guid(),
                        LastModifiedDateTime = c.DateTime(),
                        DeletedById = c.Guid(),
                        DeletedDateTime = c.DateTime(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreatedById)
                .ForeignKey("dbo.User", t => t.DeletedById)
                .ForeignKey("dbo.User", t => t.LastModifiedById)
                .Index(t => t.CreatedById)
                .Index(t => t.LastModifiedById)
                .Index(t => t.DeletedById);
            
            CreateTable(
                "dbo.BrandMeta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BrandId = c.Int(nullable: false),
                        Key = c.Int(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Brand", t => t.BrandId, cascadeDelete: true)
                .Index(t => t.BrandId);
            
            CreateTable(
                "dbo.CategoryProductAssociation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Category", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Product", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.CategoryId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentCategoryId = c.Int(),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(),
                        FinishDate = c.DateTime(),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(maxLength: 512),
                        Content = c.String(),
                        SortOrder = c.Int(nullable: false),
                        Image = c.String(maxLength: 256),
                        ImageAlt = c.String(maxLength: 256),
                        CreatedById = c.Guid(nullable: false),
                        CreatedDateTime = c.DateTime(nullable: false),
                        LastModifiedById = c.Guid(),
                        LastModifiedDateTime = c.DateTime(),
                        DeletedById = c.Guid(),
                        DeletedDateTime = c.DateTime(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreatedById)
                .ForeignKey("dbo.User", t => t.DeletedById)
                .ForeignKey("dbo.User", t => t.LastModifiedById)
                .ForeignKey("dbo.Category", t => t.ParentCategoryId)
                .Index(t => t.ParentCategoryId)
                .Index(t => t.CreatedById)
                .Index(t => t.LastModifiedById)
                .Index(t => t.DeletedById);
            
            CreateTable(
                "dbo.CategoryMeta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        Key = c.Int(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Category", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.ProductMedia",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        Value = c.String(nullable: false, maxLength: 256),
                        SortOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Product", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.ProductMeta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        Key = c.Int(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Product", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.ProductVariation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        ProductAttributeId = c.Int(nullable: false),
                        AttributeValueId = c.Int(nullable: false),
                        PriceModifier = c.Decimal(storeType: "money"),
                        DeliveryModifier = c.Decimal(storeType: "money"),
                        WeightModifier = c.Decimal(precision: 18, scale: 2),
                        VatModifier = c.Decimal(storeType: "money"),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Product", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.ProductAttribute", t => t.ProductAttributeId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.ProductAttributeId);
            
            CreateTable(
                "dbo.ProductAttribute",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AttributeId = c.Int(nullable: false),
                        Required = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attribute", t => t.AttributeId, cascadeDelete: true)
                .Index(t => t.AttributeId);
            
            CreateTable(
                "dbo.Attribute",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Style = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RelatedProduct",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        PrimaryProductId = c.Int(nullable: false),
                        SecondaryProductId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Product", t => t.PrimaryProductId, cascadeDelete: true)
                .ForeignKey("dbo.Product", t => t.SecondaryProductId)
                .Index(t => t.PrimaryProductId)
                .Index(t => t.SecondaryProductId);
            
            CreateTable(
                "dbo.Transaction",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.Guid(nullable: false),
                        UniqueCode = c.String(nullable: false, maxLength: 128),
                        IpAddress = c.String(nullable: false, maxLength: 16),
                        SessionId = c.String(nullable: false, maxLength: 32),
                        TransactionDateTime = c.DateTime(nullable: false),
                        EncryptionString = c.String(nullable: false),
                        NetAmount = c.Decimal(nullable: false, storeType: "money"),
                        TaxAmount = c.Decimal(nullable: false, storeType: "money"),
                        DeliveryAmount = c.Decimal(nullable: false, storeType: "money"),
                        GrossAmount = c.Decimal(nullable: false, storeType: "money"),
                        AuthorisedAmount = c.Decimal(nullable: false, storeType: "money"),
                        DiscountAmount = c.Decimal(storeType: "money"),
                        Currency = c.String(nullable: false, maxLength: 50),
                        Authorised = c.Boolean(nullable: false),
                        AuthorisationCode = c.String(maxLength: 128),
                        AuthorisedDateTime = c.DateTime(),
                        ManuallyAuthorised = c.Boolean(nullable: false),
                        ManuallyAuthorisedById = c.Guid(),
                        Order_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.ManuallyAuthorisedById)
                .ForeignKey("dbo.Order", t => t.Order_Id)
                .Index(t => t.ManuallyAuthorisedById)
                .Index(t => t.Order_Id);
            
            CreateTable(
                "dbo.Banner",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Image = c.String(nullable: false, maxLength: 256),
                        ImageAlt = c.String(nullable: false, maxLength: 256),
                        Position = c.Int(nullable: false),
                        CreatedById = c.Guid(nullable: false),
                        CreatedDateTime = c.DateTime(nullable: false),
                        LastModifiedById = c.Guid(),
                        LastModifiedDateTime = c.DateTime(),
                        DeletedById = c.Guid(),
                        DeletedDateTime = c.DateTime(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreatedById)
                .ForeignKey("dbo.User", t => t.DeletedById)
                .ForeignKey("dbo.User", t => t.LastModifiedById)
                .Index(t => t.CreatedById)
                .Index(t => t.LastModifiedById)
                .Index(t => t.DeletedById);
            
            CreateTable(
                "dbo.Page",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentPageId = c.Int(),
                        Status = c.Int(nullable: false),
                        StartDate = c.DateTime(),
                        FinishDate = c.DateTime(),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(maxLength: 512),
                        Content = c.String(),
                        SortOrder = c.Int(nullable: false),
                        Image = c.String(maxLength: 256),
                        ImageAlt = c.String(maxLength: 256),
                        CreatedById = c.Guid(nullable: false),
                        CreatedDateTime = c.DateTime(nullable: false),
                        LastModifiedById = c.Guid(),
                        LastModifiedDateTime = c.DateTime(),
                        DeletedById = c.Guid(),
                        DeletedDateTime = c.DateTime(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreatedById)
                .ForeignKey("dbo.User", t => t.DeletedById)
                .ForeignKey("dbo.User", t => t.LastModifiedById)
                .ForeignKey("dbo.Page", t => t.ParentPageId)
                .Index(t => t.ParentPageId)
                .Index(t => t.CreatedById)
                .Index(t => t.LastModifiedById)
                .Index(t => t.DeletedById);
            
            CreateTable(
                "dbo.PageMeta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PageId = c.Int(nullable: false),
                        Key = c.Int(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Page", t => t.PageId, cascadeDelete: true)
                .Index(t => t.PageId);
            
            CreateTable(
                "dbo.Promotion",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 128),
                        Code = c.String(nullable: false, maxLength: 128),
                        DiscountValue = c.Decimal(storeType: "money"),
                        DiscountPercent = c.Decimal(precision: 18, scale: 2),
                        StartDate = c.DateTime(),
                        FinishDate = c.DateTime(),
                        Usages = c.Int(),
                        UserId = c.Guid(),
                        Email = c.String(maxLength: 256),
                        CreatedById = c.Guid(nullable: false),
                        CreatedDateTime = c.DateTime(nullable: false),
                        LastModifiedById = c.Guid(),
                        LastModifiedDateTime = c.DateTime(),
                        DeletedById = c.Guid(),
                        DeletedDateTime = c.DateTime(),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreatedById)
                .ForeignKey("dbo.User", t => t.DeletedById)
                .ForeignKey("dbo.User", t => t.LastModifiedById)
                .Index(t => t.CreatedById)
                .Index(t => t.LastModifiedById)
                .Index(t => t.DeletedById);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Promotion", "LastModifiedById", "dbo.User");
            DropForeignKey("dbo.Promotion", "DeletedById", "dbo.User");
            DropForeignKey("dbo.Promotion", "CreatedById", "dbo.User");
            DropForeignKey("dbo.Page", "ParentPageId", "dbo.Page");
            DropForeignKey("dbo.PageMeta", "PageId", "dbo.Page");
            DropForeignKey("dbo.Page", "LastModifiedById", "dbo.User");
            DropForeignKey("dbo.Page", "DeletedById", "dbo.User");
            DropForeignKey("dbo.Page", "CreatedById", "dbo.User");
            DropForeignKey("dbo.Banner", "LastModifiedById", "dbo.User");
            DropForeignKey("dbo.Banner", "DeletedById", "dbo.User");
            DropForeignKey("dbo.Banner", "CreatedById", "dbo.User");
            DropForeignKey("dbo.Order", "UserId", "dbo.User");
            DropForeignKey("dbo.Transaction", "Order_Id", "dbo.Order");
            DropForeignKey("dbo.Transaction", "ManuallyAuthorisedById", "dbo.User");
            DropForeignKey("dbo.Order", "SignedOffById", "dbo.User");
            DropForeignKey("dbo.RelatedProduct", "SecondaryProductId", "dbo.Product");
            DropForeignKey("dbo.RelatedProduct", "PrimaryProductId", "dbo.Product");
            DropForeignKey("dbo.ProductVariation", "ProductAttributeId", "dbo.ProductAttribute");
            DropForeignKey("dbo.ProductAttribute", "AttributeId", "dbo.Attribute");
            DropForeignKey("dbo.ProductVariation", "ProductId", "dbo.Product");
            DropForeignKey("dbo.ProductMeta", "ProductId", "dbo.Product");
            DropForeignKey("dbo.ProductMedia", "ProductId", "dbo.Product");
            DropForeignKey("dbo.OrderItem", "ProductId", "dbo.Product");
            DropForeignKey("dbo.Product", "LastModifiedById", "dbo.User");
            DropForeignKey("dbo.Product", "DeletedById", "dbo.User");
            DropForeignKey("dbo.Product", "CreatedById", "dbo.User");
            DropForeignKey("dbo.CategoryProductAssociation", "ProductId", "dbo.Product");
            DropForeignKey("dbo.Category", "ParentCategoryId", "dbo.Category");
            DropForeignKey("dbo.Category", "LastModifiedById", "dbo.User");
            DropForeignKey("dbo.Category", "DeletedById", "dbo.User");
            DropForeignKey("dbo.Category", "CreatedById", "dbo.User");
            DropForeignKey("dbo.CategoryProductAssociation", "CategoryId", "dbo.Category");
            DropForeignKey("dbo.CategoryMeta", "CategoryId", "dbo.Category");
            DropForeignKey("dbo.Product", "BrandId", "dbo.Brand");
            DropForeignKey("dbo.Brand", "LastModifiedById", "dbo.User");
            DropForeignKey("dbo.Brand", "DeletedById", "dbo.User");
            DropForeignKey("dbo.Brand", "CreatedById", "dbo.User");
            DropForeignKey("dbo.BrandMeta", "BrandId", "dbo.Brand");
            DropForeignKey("dbo.OrderItem", "OrderId", "dbo.Order");
            DropForeignKey("dbo.Order", "CourierId", "dbo.Courier");
            DropForeignKey("dbo.Courier", "LastModifiedById", "dbo.User");
            DropForeignKey("dbo.Courier", "DeletedById", "dbo.User");
            DropForeignKey("dbo.Courier", "CreatedById", "dbo.User");
            DropForeignKey("dbo.UserRole", "UserId", "dbo.User");
            DropForeignKey("dbo.Member", "UserId", "dbo.User");
            DropForeignKey("dbo.MemberMeta", "MemberUserId", "dbo.Member");
            DropForeignKey("dbo.UserLogin", "UserId", "dbo.User");
            DropForeignKey("dbo.UserClaim", "UserId", "dbo.User");
            DropForeignKey("dbo.Address", "UserId", "dbo.User");
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.Role");
            DropIndex("dbo.Promotion", new[] { "DeletedById" });
            DropIndex("dbo.Promotion", new[] { "LastModifiedById" });
            DropIndex("dbo.Promotion", new[] { "CreatedById" });
            DropIndex("dbo.PageMeta", new[] { "PageId" });
            DropIndex("dbo.Page", new[] { "DeletedById" });
            DropIndex("dbo.Page", new[] { "LastModifiedById" });
            DropIndex("dbo.Page", new[] { "CreatedById" });
            DropIndex("dbo.Page", new[] { "ParentPageId" });
            DropIndex("dbo.Banner", new[] { "DeletedById" });
            DropIndex("dbo.Banner", new[] { "LastModifiedById" });
            DropIndex("dbo.Banner", new[] { "CreatedById" });
            DropIndex("dbo.Transaction", new[] { "Order_Id" });
            DropIndex("dbo.Transaction", new[] { "ManuallyAuthorisedById" });
            DropIndex("dbo.RelatedProduct", new[] { "SecondaryProductId" });
            DropIndex("dbo.RelatedProduct", new[] { "PrimaryProductId" });
            DropIndex("dbo.ProductAttribute", new[] { "AttributeId" });
            DropIndex("dbo.ProductVariation", new[] { "ProductAttributeId" });
            DropIndex("dbo.ProductVariation", new[] { "ProductId" });
            DropIndex("dbo.ProductMeta", new[] { "ProductId" });
            DropIndex("dbo.ProductMedia", new[] { "ProductId" });
            DropIndex("dbo.CategoryMeta", new[] { "CategoryId" });
            DropIndex("dbo.Category", new[] { "DeletedById" });
            DropIndex("dbo.Category", new[] { "LastModifiedById" });
            DropIndex("dbo.Category", new[] { "CreatedById" });
            DropIndex("dbo.Category", new[] { "ParentCategoryId" });
            DropIndex("dbo.CategoryProductAssociation", new[] { "ProductId" });
            DropIndex("dbo.CategoryProductAssociation", new[] { "CategoryId" });
            DropIndex("dbo.BrandMeta", new[] { "BrandId" });
            DropIndex("dbo.Brand", new[] { "DeletedById" });
            DropIndex("dbo.Brand", new[] { "LastModifiedById" });
            DropIndex("dbo.Brand", new[] { "CreatedById" });
            DropIndex("dbo.Product", new[] { "DeletedById" });
            DropIndex("dbo.Product", new[] { "LastModifiedById" });
            DropIndex("dbo.Product", new[] { "CreatedById" });
            DropIndex("dbo.Product", new[] { "BrandId" });
            DropIndex("dbo.OrderItem", new[] { "ProductId" });
            DropIndex("dbo.OrderItem", new[] { "OrderId" });
            DropIndex("dbo.Courier", new[] { "DeletedById" });
            DropIndex("dbo.Courier", new[] { "LastModifiedById" });
            DropIndex("dbo.Courier", new[] { "CreatedById" });
            DropIndex("dbo.Order", new[] { "CourierId" });
            DropIndex("dbo.Order", new[] { "SignedOffById" });
            DropIndex("dbo.Order", new[] { "UserId" });
            DropIndex("dbo.MemberMeta", new[] { "MemberUserId" });
            DropIndex("dbo.Member", new[] { "UserId" });
            DropIndex("dbo.UserLogin", new[] { "UserId" });
            DropIndex("dbo.UserClaim", new[] { "UserId" });
            DropIndex("dbo.Address", new[] { "UserId" });
            DropIndex("dbo.User", "UserNameIndex");
            DropIndex("dbo.UserRole", new[] { "RoleId" });
            DropIndex("dbo.UserRole", new[] { "UserId" });
            DropIndex("dbo.Role", "RoleNameIndex");
            DropTable("dbo.Promotion");
            DropTable("dbo.PageMeta");
            DropTable("dbo.Page");
            DropTable("dbo.Banner");
            DropTable("dbo.Transaction");
            DropTable("dbo.RelatedProduct");
            DropTable("dbo.Attribute");
            DropTable("dbo.ProductAttribute");
            DropTable("dbo.ProductVariation");
            DropTable("dbo.ProductMeta");
            DropTable("dbo.ProductMedia");
            DropTable("dbo.CategoryMeta");
            DropTable("dbo.Category");
            DropTable("dbo.CategoryProductAssociation");
            DropTable("dbo.BrandMeta");
            DropTable("dbo.Brand");
            DropTable("dbo.Product");
            DropTable("dbo.OrderItem");
            DropTable("dbo.Courier");
            DropTable("dbo.Order");
            DropTable("dbo.MemberMeta");
            DropTable("dbo.Member");
            DropTable("dbo.UserLogin");
            DropTable("dbo.UserClaim");
            DropTable("dbo.Address");
            DropTable("dbo.User");
            DropTable("dbo.UserRole");
            DropTable("dbo.Role");
        }
    }
}
