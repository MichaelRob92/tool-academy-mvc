namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductVat : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Product", "VatExempt", c => c.Boolean(nullable: false));
            AddColumn("dbo.Product", "VatRate", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Product", "VatRate");
            DropColumn("dbo.Product", "VatExempt");
        }
    }
}
