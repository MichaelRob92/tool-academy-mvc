namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductAvailability : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Availability",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        AvailabilitySchema = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Product", "AvailabilityId", c => c.Int(nullable: false));
            CreateIndex("dbo.Product", "AvailabilityId");
            AddForeignKey("dbo.Product", "AvailabilityId", "dbo.Availability", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Product", "AvailabilityId", "dbo.Availability");
            DropIndex("dbo.Product", new[] { "AvailabilityId" });
            DropColumn("dbo.Product", "AvailabilityId");
            DropTable("dbo.Availability");
        }
    }
}
