namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NavigationMenuLinkName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NavigationMenuLink", "Name", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.NavigationMenuLink", "Name");
        }
    }
}
