namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductImageUrl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Product", "ImageUrl", c => c.String(nullable: false, maxLength: 1024));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Product", "ImageUrl");
        }
    }
}
