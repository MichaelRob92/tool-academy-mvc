namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBannerSortOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Banner", "SortOrder", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Banner", "SortOrder");
        }
    }
}
