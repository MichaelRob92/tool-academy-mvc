using System;
using System.Collections.Generic;
using System.Linq;
using Total.ToolAcademy.Entity;

namespace Total.ToolAcademy.Context.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ToolAcademyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Total.ToolAcademy.Context.ToolAcademyContext";
        }

        protected override void Seed(ToolAcademyContext context)
        {
            //  This method will be called after migrating to the latest version.

            // Add user
            var userId = Guid.NewGuid();
            context.Users.AddOrUpdate(
                u => u.UserName,
                new User {Id = userId, UserName ="michael.robinson@totalsolutions.co.uk", Email = "michael.robinson@totalsolutions.co.uk"}
            );
            context.SaveChanges();

            var user = context.Users.FirstOrDefault(u => u.UserName == "michael.robinson@totalsolutions.co.uk");
            if (user != null)
                userId = user.Id;

            // Add role
            var roleId = Guid.NewGuid();
            context.Roles.AddOrUpdate(
                r => r.Name,
                new Role { Id = roleId, Name = "Total" },
                new Role { Id = Guid.NewGuid(), Name = "Customer" }
            );
            context.SaveChanges();

            var role = context.Roles.FirstOrDefault(u => u.Name == "Total");
            if (role != null)
                roleId = role.Id;

            // Add user role
            context.UserRoles.AddOrUpdate(
                r => r.UserId,
                new GuidUserRole { RoleId = roleId, UserId = userId}
            );

            // Add main banner
            context.Banners.AddOrUpdate(
                b => b.Name,
                new Banner
                {
                    Name = "Main Feature", 
                    Image = "main-banner.jpg", 
                    ImageAlt = "Main Banner", 
                    Position = BannerPosition.MainFeature, 
                    SortOrder = 0, 
                    CreatedById = userId, 
                    CreatedDateTime = DateTime.Now,
                    Deleted = false
                }
            );

            // Add main brand
            context.Brands.AddOrUpdate(
                b => b.Name,
                new Brand 
                { 
                    Name = "Main Brand", 
                    Permalink = "main-brand", 
                    Status = BrandStatus.Live, 
                    SortOrder = 0, 
                    Description = "Main Brand", 
                    Content = "<p>Main Brand</p>", 
                    Image = "main-brand.jpg", 
                    ImageAlt = "Main Brand", 
                    CreatedById = userId, 
                    CreatedDateTime = DateTime.Now, 
                    Deleted = false
                }
            );
            context.SaveChanges();

            var brand = context.Brands.FirstOrDefault(b => b.Name == "Main Brand");
            var brandId = -1;
            if (brand != null)
                brandId = brand.Id;

            // Can't do anymore without a brand
            if (brandId == -1)
                return;

            // Add availabilities
            var availabilities = new List<Availability>
            {
                new Availability
                {
                    Name = "Discontinued",
                    AvailabilitySchema = AvailabilitySchema.Discontinued
                },
                new Availability
                {
                    Name = "In Stock",
                    AvailabilitySchema = AvailabilitySchema.InStock
                },
                new Availability
                {
                    Name = "In Store Only",
                    AvailabilitySchema = AvailabilitySchema.InStoreOnly
                },
                new Availability
                {
                    Name = "Limited Availability",
                    AvailabilitySchema = AvailabilitySchema.LimitedAvailability
                },
                new Availability
                {
                    Name = "Online Only",
                    AvailabilitySchema = AvailabilitySchema.OnlineOnly
                },
                new Availability
                {
                    Name = "Out Of Stock",
                    AvailabilitySchema = AvailabilitySchema.OutOfStock
                },
                new Availability
                {
                    Name = "Pre Order",
                    AvailabilitySchema = AvailabilitySchema.PreOrder
                },
                new Availability
                {
                    Name = "Sold Out",
                    AvailabilitySchema = AvailabilitySchema.SoldOut
                }
            };

            context.Availabilities.AddOrUpdate(
                e => e.Name,
                availabilities.ToArray()
            );
            context.SaveChanges();

            var availability = context.Availabilities.FirstOrDefault(e => e.Name == "In Stock");
            var availabilityId = -1;
            if (availability != null)
                availabilityId = availability.Id;

            if (availabilityId == -1)
                return;

            context.SaveChanges();
            context.Dispose();

            context = new ToolAcademyContext();
            context.Configuration.AutoDetectChangesEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;

            for (var index = 1; index <= 10000; index++)
            {
                var product = new Product
                {
                    Name = string.Format("Product {0}", index),
                    Permalink = string.Format("product-{0}", index),
                    Code = index.ToString("D6"),
                    Price = 9.99m,
                    OfferPrice = 8.99m,
                    VatExempt = false,
                    Summary = "Product",
                    Content = "Product",
                    Featured = false,
                    SortOrder = index,
                    Status = ProductStatus.Live,
                    Weight = 0.0m,
                    Discontinued = false,
                    SpecialOffer = false,
                    BrandId = brandId,
                    AvailabilityId = availabilityId,
                    CreatedById = userId,
                    CreatedDateTime = DateTime.Now,
                    Deleted = false
                };

                context.Products.AddOrUpdate(
                    p => p.Name,
                    product
                );

                if (index%100 != 0) continue;

                context.SaveChanges();
                context.Dispose();

                context = new ToolAcademyContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
            }

            context.SaveChanges();
            context.Dispose();

            context = new ToolAcademyContext();
            context.Configuration.AutoDetectChangesEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;

            for (var index = 1; index <= 10000; index++)
            {
                var specifications = new List<ProductSpecification>
                {
                    new ProductSpecification
                    {
                        ProductId = index,
                        Title = "Product Specification",
                        Content = "Product Specification",
                        SortOrder = 0
                    }
                };
                context.ProductSpecifications.AddOrUpdate(
                    p => p.ProductId,
                    specifications.ToArray()
                );

                if (index%100 != 0) continue;

                context.SaveChanges();
                context.Dispose();

                context = new ToolAcademyContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
            }

            context.SaveChanges();
            context.Dispose();

            context = new ToolAcademyContext();

            // Menu
            var navigationMenu = new NavigationMenu
            {
                Name = "Main Menu",
                Location = NavigationMenuLocation.MainMenu,
                CreatedById = userId,
                CreatedDateTime = DateTime.Now,
                LastModifiedById = userId,
                LastModifiedDateTime = DateTime.Now,
                Deleted = false
            };

            context.NavigationMenus.AddOrUpdate(
                e => e.Name,
                navigationMenu
            );

            context.SaveChanges();

            navigationMenu = context.NavigationMenus.FirstOrDefault(n => n.Location == NavigationMenuLocation.MainMenu);
            if (navigationMenu != null)
            {
                var menuLinks = new List<NavigationMenuLink>
                {
                    new NavigationMenuLink
                    {
                        Link = "/",
                        Name = "Home",
                        NavigationMenuId = navigationMenu.Id,
                        SortOrder = 0
                    }
                };
                context.NavigationMenuLinks.AddOrUpdate(
                    n => n.Name,
                    menuLinks.ToArray()
                );
            }

            context.SaveChanges();

        }
    }
}
