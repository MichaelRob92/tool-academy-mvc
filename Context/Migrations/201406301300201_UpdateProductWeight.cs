namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProductWeight : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AttributeValue",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AttributeId = c.Int(nullable: false),
                        Value = c.String(nullable: false, maxLength: 256),
                        Image = c.String(maxLength: 256),
                        ImageAlt = c.String(maxLength: 256),
                        SortOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attribute", t => t.AttributeId, cascadeDelete: true)
                .Index(t => t.AttributeId);
            
            CreateTable(
                "dbo.BasketProduct",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BasketId = c.Guid(nullable: false),
                        ProductId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Basket", t => t.BasketId, cascadeDelete: true)
                .ForeignKey("dbo.Product", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.BasketId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Basket",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Date = c.DateTime(nullable: false),
                        UserId = c.Guid(),
                        IpAddress = c.String(nullable: false, maxLength: 16),
                        SessionId = c.String(nullable: false, maxLength: 32),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BasketProductVariation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BasketProductId = c.Int(nullable: false),
                        PublicVariationId = c.Int(nullable: false),
                        ProductVariation_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BasketProduct", t => t.BasketProductId, cascadeDelete: true)
                .ForeignKey("dbo.ProductVariation", t => t.ProductVariation_Id)
                .Index(t => t.BasketProductId)
                .Index(t => t.ProductVariation_Id);
            
            CreateTable(
                "dbo.MailingList",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmailAddress = c.String(nullable: false, maxLength: 256),
                        UserId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId);
            
            AlterColumn("dbo.Product", "Weight", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MailingList", "UserId", "dbo.User");
            DropForeignKey("dbo.BasketProduct", "ProductId", "dbo.Product");
            DropForeignKey("dbo.BasketProductVariation", "ProductVariation_Id", "dbo.ProductVariation");
            DropForeignKey("dbo.BasketProductVariation", "BasketProductId", "dbo.BasketProduct");
            DropForeignKey("dbo.BasketProduct", "BasketId", "dbo.Basket");
            DropForeignKey("dbo.AttributeValue", "AttributeId", "dbo.Attribute");
            DropIndex("dbo.MailingList", new[] { "UserId" });
            DropIndex("dbo.BasketProductVariation", new[] { "ProductVariation_Id" });
            DropIndex("dbo.BasketProductVariation", new[] { "BasketProductId" });
            DropIndex("dbo.BasketProduct", new[] { "ProductId" });
            DropIndex("dbo.BasketProduct", new[] { "BasketId" });
            DropIndex("dbo.AttributeValue", new[] { "AttributeId" });
            AlterColumn("dbo.Product", "Weight", c => c.Decimal(precision: 18, scale: 2));
            DropTable("dbo.MailingList");
            DropTable("dbo.BasketProductVariation");
            DropTable("dbo.Basket");
            DropTable("dbo.BasketProduct");
            DropTable("dbo.AttributeValue");
        }
    }
}
