namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PendingChanges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductSpecification", "ProductId", "dbo.Product");
            AddColumn("dbo.ProductSpecification", "Product_Id", c => c.Int());
            CreateIndex("dbo.ProductSpecification", "Product_Id");
            AddForeignKey("dbo.ProductSpecification", "Product_Id", "dbo.Product", "Id");
            AddForeignKey("dbo.ProductSpecification", "ProductId", "dbo.Product", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductSpecification", "ProductId", "dbo.Product");
            DropForeignKey("dbo.ProductSpecification", "Product_Id", "dbo.Product");
            DropIndex("dbo.ProductSpecification", new[] { "Product_Id" });
            DropColumn("dbo.ProductSpecification", "Product_Id");
            AddForeignKey("dbo.ProductSpecification", "ProductId", "dbo.Product", "Id", cascadeDelete: true);
        }
    }
}
