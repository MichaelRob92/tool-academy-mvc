namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBannerUrl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Banner", "Url", c => c.String(maxLength: 512));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Banner", "Url");
        }
    }
}
