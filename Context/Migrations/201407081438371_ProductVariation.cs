namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductVariation : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.ProductVariation", "AttributeValueId");
            AddForeignKey("dbo.ProductVariation", "AttributeValueId", "dbo.AttributeValue", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductVariation", "AttributeValueId", "dbo.AttributeValue");
            DropIndex("dbo.ProductVariation", new[] { "AttributeValueId" });
        }
    }
}
