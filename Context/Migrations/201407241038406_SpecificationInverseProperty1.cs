namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SpecificationInverseProperty1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductSpecification", "ProductId", "dbo.Product");
            AddForeignKey("dbo.ProductSpecification", "ProductId", "dbo.Product", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductSpecification", "ProductId", "dbo.Product");
            AddForeignKey("dbo.ProductSpecification", "ProductId", "dbo.Product", "Id");
        }
    }
}
