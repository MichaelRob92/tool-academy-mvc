namespace Total.ToolAcademy.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductImageUrlRemove : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Product", "ImageUrl");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Product", "ImageUrl", c => c.String(nullable: false, maxLength: 1024));
        }
    }
}
