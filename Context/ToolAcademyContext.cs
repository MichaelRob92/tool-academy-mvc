﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;
using Total.ToolAcademy.Entity;
using Total.ToolAcademy.Framework;

namespace Total.ToolAcademy.Context
{
    public class ToolAcademyContext : IdentityDbContext<User, Role, Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>, IDbContext
    {
        public ToolAcademyContext() : base("name=ToolAcademyConnection")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public DbContext DbContext
        {
            get { return this; }
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Role>().ToTable("Role");
            modelBuilder.Entity<GuidUserRole>().ToTable("UserRole");
            modelBuilder.Entity<GuidUserLogin>().ToTable("UserLogin");
            modelBuilder.Entity<GuidUserClaim>().ToTable("UserClaim");

            modelBuilder.Entity<User>().HasOptional(e => e.Member).WithRequired(e => e.User);
            modelBuilder.Entity<Order>().HasOptional(e => e.Transaction).WithRequired(e => e.Order);

            // User cascade
            modelBuilder.Entity<Banner>().HasRequired(e => e.CreatedBy).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Brand>().HasRequired(e => e.CreatedBy).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Category>().HasRequired(e => e.CreatedBy).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Courier>().HasRequired(e => e.CreatedBy).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Page>().HasRequired(e => e.CreatedBy).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Product>().HasRequired(e => e.CreatedBy).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Promotion>().HasRequired(e => e.CreatedBy).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<NavigationMenu>().HasRequired(e => e.CreatedBy).WithMany().WillCascadeOnDelete(false);

            // Product cascade
            modelBuilder.Entity<RelatedProduct>().HasRequired(e => e.SecondaryProduct).WithMany(p => p.RelatedProducts).WillCascadeOnDelete(false);
            modelBuilder.Entity<BasketProductVariation>().HasRequired(e => e.ProductVariation).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<ProductVariation>().HasRequired(e => e.AttributeValue).WithMany().WillCascadeOnDelete(false);
            //modelBuilder.Entity<ProductSpecification>().HasRequired(e => e.Product).WithMany(p => p.ProductSpecifications).WillCascadeOnDelete(false);
            //modelBuilder.Entity<Product>().HasMany(e => e.ProductSpecifications).WithRequired(p => p.Product).WillCascadeOnDelete(false);
        }

        public virtual IDbSet<Address> Addresses { get; set; }
        public virtual IDbSet<Entity.Attribute> Attributes { get; set; }
        public virtual IDbSet<AttributeValue> AttributeValues { get; set; }
        public virtual IDbSet<Availability> Availabilities { get; set; } 
        public virtual IDbSet<Banner> Banners { get; set; }
        public virtual IDbSet<Basket> Baskets { get; set; }
        public virtual IDbSet<BasketProduct> BasketProducts { get; set; }
        public virtual IDbSet<Brand> Brands { get; set; }
        public virtual IDbSet<Category> Categories { get; set; }
        public virtual IDbSet<CategoryProductAssociation> CategoryProductAssociations { get; set; }
        public virtual IDbSet<Courier> Couriers { get; set; }
        public virtual IDbSet<NavigationMenu> NavigationMenus { get; set; }
        public virtual IDbSet<NavigationMenuLink> NavigationMenuLinks { get; set; } 
        public virtual IDbSet<MailingList> MailingLists { get; set; }
        public virtual IDbSet<Member> Members { get; set; }
        public virtual IDbSet<Order> Orders { get; set; }
        public virtual IDbSet<OrderItem> OrderItems { get; set; }
        public virtual IDbSet<Page> Pages { get; set; }
        public virtual IDbSet<Product> Products { get; set; }
        public virtual IDbSet<ProductAttribute> ProductAttributes { get; set; }
        public virtual IDbSet<ProductMedia> ProductMedia { get; set; }
        public virtual IDbSet<ProductVariation> ProductVariations { get; set; }
        public virtual IDbSet<ProductSpecification> ProductSpecifications { get; set; } 
        public virtual IDbSet<Promotion> Promotions { get; set; }
        public virtual IDbSet<RelatedProduct> RelatedProducts { get; set; }
        public virtual IDbSet<Transaction> Transactions { get; set; }
        public virtual IDbSet<GuidUserRole> UserRoles { get; set; }
        public virtual IDbSet<SystemLog> SystemLogs { get; set; }
        public virtual IDbSet<SystemMessage> SystemMessages { get; set; }
    }
}
