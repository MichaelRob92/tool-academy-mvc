﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Total.ToolAcademy.Framework;

namespace Total.ToolAcademy.Context
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly IDbContext _context;

        public Repository(IDbContext context)
        {
            _context = context;
        }

        public virtual IDbSet<T> Set
        {
            get { return _context.Set<T>();}
        }

        public void Attach(T entity)
        {
            Set.Attach(entity);
        }

        public async void Add(params T[] entities)
        {
            foreach (var entity in entities)
                Set.Add(entity);
            await _context.SaveChangesAsync();
        }

        public T First(Expression<Func<T, bool>> predicate)
        {
            return Set.First(predicate);
        }

        public T FirstOrDefault(Expression<Func<T, bool>> predicate)
        {
            return Set.FirstOrDefault(predicate);
        }

        public async Task<T> FirstAsync(Expression<Func<T, bool>> predicate)
        {
            return await Set.FirstAsync(predicate);
        }

        public async Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate)
        {
            return await Set.FirstOrDefaultAsync(predicate);
        }

        public async Task<int> CountAsync()
        {
            return await Set.CountAsync();
        }

        public async Task<int> CountAsync(Expression<Func<T, bool>> predicate)
        {
            return await Set.CountAsync(predicate);
        }

        public async Task<bool> AnyAsync(Expression<Func<T, bool>> predicate)
        {
            return await Set.AnyAsync(predicate);
        }

        public IQueryable<T> Where(Expression<Func<T, bool>> predicate)
        {
            return Set.Where(predicate);
        }

        public IQueryable<T> Where<TProperty>(Expression<Func<T, bool>> predicate, Expression<Func<T, TProperty>> order)
        {
            return Set.Where(predicate).OrderBy(order);
        }

        public IQueryable<T> Where(Expression<Func<T, bool>> predicate, int count)
        {
            return Set.Where(predicate).Take(count);
        }

        public IQueryable<T> Where<TProperty>(Expression<Func<T, bool>> predicate, Expression<Func<T, TProperty>> order, int count)
        {
            return Set.Where(predicate).OrderBy(order).Take(count);
        }

        public IQueryable<TResult> Select<TResult>(Expression<Func<T, TResult>> selector)
        {
            return Set.Select(selector);
        }

        public IQueryable<TResult> Select<TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector)
        {
            return Set.Where(predicate).Select(selector);
        }

        public IQueryable<TResult> Select<TResult, TOrder>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector, Expression<Func<T, TOrder>> order)
        {
            return Set.Where(predicate).OrderBy(order).Select(selector);
        }

        public IQueryable<T> Include(params string[] includes)
        {
            var query = Set.AsQueryable();
            return includes.Aggregate(query, (q, i) => q.Include(i));
        }

        public IQueryable<T> Include<TProperty>(params Expression<Func<T, TProperty>>[] includes)
        {
            var query = Set.AsQueryable();
            return includes.Aggregate(query, (q, i) => q.Include(i));
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing || _context == null)
                return;

            _context.Dispose();
        }
    }
}
