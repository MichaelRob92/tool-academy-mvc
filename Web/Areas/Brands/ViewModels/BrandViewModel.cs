﻿namespace Total.ToolAcademy.Web.Areas.Brands.ViewModels
{
    public class BrandViewModel
    {
        // Core
        public string Name { get; set; }
        public string Permalink { get; set; }
        public string Image { get; set; }
        public string ImageAlt { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }

        // Meta
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
    }
}