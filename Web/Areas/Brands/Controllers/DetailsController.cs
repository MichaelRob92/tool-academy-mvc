﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Castle.Core.Logging;
using Total.ToolAcademy.Framework;
using Total.ToolAcademy.Helpers;
using Total.ToolAcademy.Web.Areas.Brands.ViewModels;

namespace Total.ToolAcademy.Web.Areas.Brands.Controllers
{
    public class DetailsController : Controller
    {
        public ILogger Logger { get; set; }

        private readonly IDbContext _context;

        public DetailsController(IDbContext context)
        {
            _context = context;
        }

        // GET: brands/{permalink}
        public async Task<ActionResult> Index(string permalink)
        {
            permalink = permalink.ToLower();
            var brand = await _context.Brands.FirstOrDefaultAsync(b => b.Permalink == permalink);
            if (brand == null || !brand.IsValid())
            {
                Logger.WarnFormat("Status: 404 - Invalid brand found for request: {0}", HttpUtility.UrlEncode(permalink));
                return Redirect("~/404");
            }

            //var products = await _context.Products
            //    .Where(p => p.BrandId == brand.Id)
            //    .Select(p => new ProductViewModel
            //    {
            //        Code = p.Code,
            //        Name = p.Name,
            //        Summary = p.Summary,
            //        Content = p.Content,
            //        Price = p.Price,
            //        OfferPrice = p.OfferPrice,

            //        Weight = p.Weight,
            //        Discontinued = p.Discontinued,
            //        DiscontinuedMessage = p.DiscontinuedMessage,

            //        MetaTitle = p.MetaTitle,
            //        MetaDescription = p.MetaDescription
            //    })
            //    .ToListAsync();
            

            return View(new BrandViewModel { Name = brand.Name, Description = brand.Description, Image = brand.Image, ImageAlt = brand.ImageAlt, Content = brand.Content, MetaTitle = brand.MetaTitle, MetaDescription = brand.MetaDescription });
        }
    }
}