﻿using System.Web.Mvc;

namespace Total.ToolAcademy.Web.Areas.Brands
{
    public class BrandsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get { return "Brands"; }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BrandDetails",
                "brands/{permalink}",
                new { controller = "Details", action = "Index" },
                new[] { "Total.ToolAcademy.Web.Areas.Brands.Controllers" }
            );
            context.MapRoute(
                "Brands",
                "brands/{controller}/{action}",
                new { },
                new[] { "Total.ToolAcademy.Web.Areas.Brands.Controllers" }
            );
        }
    }
}