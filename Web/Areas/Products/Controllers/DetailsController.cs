﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Castle.Core.Logging;
using Total.ToolAcademy.Framework;
using Total.ToolAcademy.Helpers;
using Total.ToolAcademy.Web.Areas.Products.ViewModels;

namespace Total.ToolAcademy.Web.Areas.Products.Controllers
{
    public class DetailsController : Controller
    {
        public ILogger Logger { get; set; }

        private readonly IDbContext _context;

        public DetailsController(IDbContext context)
        {
            _context = context;
        }

        //[OutputCache(Duration = 900, VaryByParam = "permalink")]
        // GET: product/details/{permalink}
        public async Task<ActionResult> Index(string permalink)
        {
            permalink = permalink.ToLower();

            var product = await _context.Products.Include(p => p.Availability).FirstOrDefaultAsync(p => p.Permalink == permalink);
            if (product == null || !product.IsValid())
            {
                Logger.WarnFormat("Status: 404 - Invalid product found for request: {0}", HttpUtility.UrlEncode(permalink));
                return Redirect("~/404");
            }

            // Get the media
            var media = await _context.ProductMedia
                .Where(p => p.ProductId == product.Id)
                .OrderBy(p => p.SortOrder)
                .Select(p => new ProductMediaViewModel
                {
                    Type = p.Type,
                    Value = p.Value
                })
                .ToListAsync();

            // Get the variations
            // TODO: Sorting
            var variations = await _context.ProductVariations
                .Where(p => p.ProductId == product.Id && p.Active)
                //.OrderBy(p => p.ProductAttribute.AttributeId)
                .Select(p => new ProductVariationViewModel
                {
                    Required = p.ProductAttribute.Required,
                    Attribute = new AttributeViewModel
                    {
                        Name = p.ProductAttribute.Attribute.Name,
                        Style = p.ProductAttribute.Attribute.Style,
                        Type = p.ProductAttribute.Attribute.Type
                    },
                    AttributeValue = new AttributeValueViewModel
                    {
                        Value = p.AttributeValue.Value,
                        Image = p.AttributeValue.Image,
                        ImageAlt = p.AttributeValue.ImageAlt,
                        SortOrder = p.AttributeValue.SortOrder
                    }
                })
                //.OrderBy(p => p.AttributeValue.SortOrder) 
                .ToListAsync();

            // Get the specifications
            var specifications = await _context.ProductSpecifications
                .Where(p => p.ProductId == product.Id)
                .OrderBy(p => p.SortOrder)
                .Select(p => new ProductSpecificationViewModel
                {
                    Title = p.Title,
                    Content = p.Content
                })
                .ToListAsync();

            var productView = new ProductViewModel
            {
                Id = product.Id,
                Code = product.Code,
                Name = product.Name,
                Summary = product.Summary,
                Content = product.Content,
                DisplayPrice = product.GetPrice(),
                Price = product.Price,
                OfferPrice = product.OfferPrice,
                VatExempt = product.VatExempt,
                VatRate = product.VatRate,
                Image = product.Image,
                ImageAlt = product.ImageAlt,

                Weight = product.Weight,
                Discontinued = product.Discontinued,
                DiscontinuedMessage = product.DiscontinuedMessage,

                MetaTitle = product.MetaTitle,
                MetaDescription = product.MetaDescription,

                Availability = new AvailabilityViewModel
                {
                    Name = product.Availability.Name,
                    Schema = product.Availability.AvailabilitySchema.ToString()
                },

                Media = media,
                Variations = variations,
                Specifications = specifications
            };

            return View(productView);
        }
    }
}