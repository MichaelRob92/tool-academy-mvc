﻿using System.Collections.Generic;
using Total.ToolAcademy.Entity;

namespace Total.ToolAcademy.Web.Areas.Products.ViewModels
{
    public class ProductViewModel
    {
        // Core
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Summary { get; set; }
        public string Content { get; set; }
        public decimal DisplayPrice { get; set; }
        public decimal Price { get; set; }
        public decimal? OfferPrice { get; set; }
        public bool VatExempt { get; set; }
        public decimal? VatRate { get; set; }
        public string Image { get; set; }
        public string ImageAlt { get; set; }

        // Misc
        public decimal Weight { get; set; }
        public bool Discontinued { get; set; }
        public string DiscontinuedMessage { get; set; }

        // Meta
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }

        // Availability
        public AvailabilityViewModel Availability { get; set; }

        // Media
        public IEnumerable<ProductMediaViewModel> Media { get; set; } 

        // Variation
        public IEnumerable<ProductVariationViewModel> Variations { get; set; } 

        // Specifcations
        public IEnumerable<ProductSpecificationViewModel> Specifications { get; set; } 
    }

    public class AvailabilityViewModel
    {
        public string Name { get; set; }

        public string Schema { get; set; }
    }

    public class ProductMediaViewModel
    {
        public ProductMediaType Type { get; set; }

        public string Value { get; set; }
    }

    public class ProductVariationViewModel
    {
        public bool Required { get; set; }

        public AttributeViewModel Attribute { get; set; }

        public AttributeValueViewModel AttributeValue { get; set; }
    }

    public class ProductSpecificationViewModel
    {
        public string Title { get; set; }

        public string Content { get; set; }
    }

    public class AttributeViewModel
    {
        public AttributeType Type { get; set; }

        public AttributeStyle Style { get; set; }

        public string Name { get; set; }
    }

    public class AttributeValueViewModel
    {
        public string Value { get; set; }

        public string Image { get; set; }

        public string ImageAlt { get; set; }

        public int SortOrder { get; set; }
    }
}