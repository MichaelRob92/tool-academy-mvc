﻿using System.Web.Mvc;

namespace Total.ToolAcademy.Web.Areas.Products
{
    public class ProductsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get { return "Products"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ProductDetails",
                "product/details/{permalink}",
                new { controller = "Details", action = "Index" },
                new[] { "Total.ToolAcademy.Web.Areas.Products.Controllers" }
            );
            context.MapRoute(
                "Products",
                "product/{controller}/{action}",
                new { },
                new[] { "Total.ToolAcademy.Web.Areas.Products.Controllers" }
            );
        }
    }
}