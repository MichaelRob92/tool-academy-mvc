﻿using System.Linq;
using System.Web.Mvc;
using Total.ToolAcademy.Framework;
using Total.ToolAcademy.Web.ViewModels;

namespace Total.ToolAcademy.Web.ActionFilters
{
    public class BasketActionFilterAttribute : ActionFilterAttribute
    {
        public IDbContext Context { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var sessionId = string.Empty;
            if (filterContext.HttpContext.Session != null)
                 sessionId = filterContext.HttpContext.Session.SessionID;

            var basket = Context.Baskets
                .Where(e => e.SessionId == sessionId)
                .Select(e => new BasketOverviewViewModel
                {
                    Count = e.BasketProducts.Sum(p => p.Quantity),
                    Total = e.BasketProducts.Sum(p => p.Price)
                })
                .FirstOrDefault() ?? new BasketOverviewViewModel();
            filterContext.Controller.ViewBag.Basket = basket;

            base.OnActionExecuting(filterContext);
        }
    }
}