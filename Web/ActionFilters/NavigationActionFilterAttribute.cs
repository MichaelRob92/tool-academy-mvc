﻿using System.Linq;
using System.Web.Mvc;
using Total.ToolAcademy.Entity;
using Total.ToolAcademy.Framework;
using Total.ToolAcademy.Web.ViewModels;

namespace Total.ToolAcademy.Web.ActionFilters
{
    public class NavigationActionFilterAttribute : ActionFilterAttribute
    {
        public IDbContext Context { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var mainNavigationViewModel = new MainNavigationViewModel
            {
                Menus = Context.NavigationMenus
                    .Where(e => e.Location == NavigationMenuLocation.MainMenu)
                    .Select(e => new NavigationMenuViewModel
                    {
                        Name = e.Name,
                        Link = e.Link,
                        Links = e.NavigationMenuLinks.Select(n => new NavigationMenuLinkViewModel
                        {
                            Name = n.Name,
                            Link = n.Link
                        })
                    })
            };
            filterContext.Controller.ViewBag.MainNavigation = mainNavigationViewModel;

            base.OnActionExecuting(filterContext);
        }
    }
}