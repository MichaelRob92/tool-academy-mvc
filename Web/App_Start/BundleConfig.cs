﻿using System.Web.Optimization;
using BundleTransformer.Core.Builders;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Transformers;

namespace Total.ToolAcademy.Web
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = true;
            bundles.UseCdn = true;

            var nullBuilder = new NullBuilder();
            var nullOrderer = new NullOrderer();
            var cssTransformer = new StyleTransformer();
            var jsTransformer = new ScriptTransformer();

            var jQueryBundle = new Bundle("~/js/jquery", "//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js");
            jQueryBundle.Include("~/Content/Scripts/Vendor/jquery-{version}.js");
            jQueryBundle.Builder = nullBuilder;
            jQueryBundle.Orderer = nullOrderer;
            jQueryBundle.Transforms.Add(jsTransformer);
            jQueryBundle.CdnFallbackExpression = "window.jQuery";
            bundles.Add(jQueryBundle);

            var modernizrBundle = new Bundle("~/js/modernizr");
            modernizrBundle.Include("~/Content/Scripts/Vendor/modernizr-{version}.min.js");
            modernizrBundle.Builder = nullBuilder;
            modernizrBundle.Orderer = nullOrderer;
            modernizrBundle.Transforms.Add(jsTransformer);
            bundles.Add(modernizrBundle);

            var bootstrapScriptBundle = new Bundle("~/js/bootstrap");
            bootstrapScriptBundle.Include(
                "~/Content/Scripts/Bootstrap/tooltip.js",
                "~/Content/Scripts/Bootstrap/affix.js",
                "~/Content/Scripts/Bootstrap/alert.js",
                "~/Content/Scripts/Bootstrap/button.js",
                "~/Content/Scripts/Bootstrap/carousel.js",
                "~/Content/Scripts/Bootstrap/collapse.js",
                "~/Content/Scripts/Bootstrap/dropdown.js",
                "~/Content/Scripts/Bootstrap/modal.js",
                "~/Content/Scripts/Bootstrap/popover.js",
                "~/Content/Scripts/Bootstrap/scrollspy.js",
                "~/Content/Scripts/Bootstrap/tab.js",
                "~/Content/Scripts/Bootstrap/transition.js"
            );
            bootstrapScriptBundle.Builder = nullBuilder;
            bootstrapScriptBundle.Orderer = nullOrderer;
            bootstrapScriptBundle.Transforms.Add(jsTransformer);
            bundles.Add(bootstrapScriptBundle);

            var bootstrapStyleBundle = new Bundle("~/css/bootstrap");
            bootstrapStyleBundle.Include("~/Content/Styles/Bootstrap/bootstrap.less");
            bootstrapStyleBundle.Builder = nullBuilder;
            bootstrapStyleBundle.Orderer = nullOrderer;
            bootstrapStyleBundle.Transforms.Add(cssTransformer);
            bundles.Add(bootstrapStyleBundle);

            var fontAwesomeBundle = new Bundle("~/css/font-awesome");
            fontAwesomeBundle.Include("~/Content/Styles/FontAwesome/font-awesome.less");
            fontAwesomeBundle.Builder = nullBuilder;
            fontAwesomeBundle.Orderer = nullOrderer;
            fontAwesomeBundle.Transforms.Add(cssTransformer);
            bundles.Add(fontAwesomeBundle);

            var mainBundle = new Bundle("~/css/main");
            mainBundle.Include("~/Content/Styles/Main/main.less");
            mainBundle.Builder = nullBuilder;
            mainBundle.Orderer = nullOrderer;
            mainBundle.Transforms.Add(cssTransformer);
            bundles.Add(mainBundle);

            var pluginBundle = new Bundle("~/js/plugins");
            pluginBundle.Include(
                "~/Content/Scripts/Vendor/jquery.validate.js",
                "~/Content/Scripts/Vendor/jquery.validate.unobtrusive.js"
            );
            pluginBundle.Builder = nullBuilder;
            pluginBundle.Orderer = nullOrderer;
            pluginBundle.Transforms.Add(jsTransformer);
            bundles.Add(pluginBundle);

            var mainScriptBundle = new Bundle("~/js/main");
            mainScriptBundle.IncludeDirectory("~/Content/Scripts/Main", "*.js");
            mainScriptBundle.Builder = nullBuilder;
            mainScriptBundle.Orderer = nullOrderer;
            mainScriptBundle.Transforms.Add(jsTransformer);
            bundles.Add(mainScriptBundle);

            var hoganBundle = new Bundle("~/js/hogan");
            hoganBundle.Include("~/Content/Scripts/Vendor/hogan-{version}.js");
            hoganBundle.Builder = nullBuilder;
            hoganBundle.Orderer = nullOrderer;
            hoganBundle.Transforms.Add(jsTransformer);
            bundles.Add(hoganBundle);

            var productSearchPageBundle = new Bundle("~/js/pages/product-search");
            productSearchPageBundle.Include("~/Content/Scripts/Main/Pages/product-search.js");
            productSearchPageBundle.Builder = nullBuilder;
            productSearchPageBundle.Orderer = nullOrderer;
            productSearchPageBundle.Transforms.Add(jsTransformer);
            bundles.Add(productSearchPageBundle);
        }
    }
}