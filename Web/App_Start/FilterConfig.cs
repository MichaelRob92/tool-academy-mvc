﻿using System.Web.Mvc;
using Total.ToolAcademy.Web.ActionFilters;

namespace Total.ToolAcademy.Web
{
    public static class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new NavigationActionFilterAttribute());
            filters.Add(new BasketActionFilterAttribute());
        }
    }
}