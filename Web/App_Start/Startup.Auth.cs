﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataProtection;
using Owin;
using Total.ToolAcademy.Context.Identity;
using Total.ToolAcademy.Entity;

namespace Total.ToolAcademy.Web
{
    public partial class Startup
    {
        internal static IDataProtectionProvider DataProtectionProvider { get; private set; }

        public void ConfigureAuth(IAppBuilder app)
        {
            DataProtectionProvider = app.GetDataProtectionProvider();

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                CookieName = "Tool Academy User",
                LoginPath = new PathString("/account/login"),
                LogoutPath = new PathString("/account/logout"),
                ReturnUrlParameter = "ref",
                SlidingExpiration = true,
                ExpireTimeSpan = TimeSpan.FromHours(2),
                Provider = new CookieAuthenticationProvider
                {
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ToolAcademyUserManager, User, Guid>(
                        TimeSpan.FromMinutes(20), 
                        (manager, user) => user.GenerateUserIdentityAsync(manager),
                        (id) => Guid.Parse(id.GetUserId())
                    )
                }
            });
        }
    }
}