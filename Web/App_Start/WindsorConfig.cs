﻿using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Total.ToolAcademy.Web.Windsor;

namespace Total.ToolAcademy.Web
{
    public static class WindsorConfig
    {
        private static IWindsorContainer _container;

        public static void RegisterContainer()
        {
            _container = new WindsorContainer().Install(FromAssembly.This());
            var controllerFactory = new WindsorControllerFactory(_container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            var compositionRoot = new WindsorCompositionRoot(_container.Kernel);
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator), compositionRoot);
        }

        public static void DisposeContainer()
        {
            _container.Dispose();
        }
    }
}