﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Total.ToolAcademy.Web
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

            routes.MapMvcAttributeRoutes();

            // Account
            //routes.MapRoute(
            //    "Account",
            //    "account/{action}",
            //    new {controller = "Account", action = "Index"}
            //);

            // Search
            routes.MapRoute(
                "Search",
                "search",
                new {controller = "Search", action = "Index"}//,
                //new {q = @"\w+"}
            );

            // Newsletter
            routes.MapRoute(
                "Newsletter",
                "newsletter/{action}",
                new {controller = "Newsletter"}
            );

            // Home route
            routes.MapRoute(
                "Home",
                "",
                new {controller = "Home", action = "Index"}
            );

            // 404 status
            routes.MapRoute("404", "404", new {controller = "Status", action = "NotFound"});

            // Anything else not found
            routes.MapRoute("NotFound", "{*url}", new {controller = "Status", action = "NotFound"});

            routes.LowercaseUrls = true;
        }
    }
}
