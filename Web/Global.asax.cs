﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Total.ToolAcademy.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            // Register filters
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            // Register routes
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            // Register bundles
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            // Register windsor
            WindsorConfig.RegisterContainer();
        }

        protected void Application_End()
        {
            // Dispose Windsor Container
            WindsorConfig.DisposeContainer();
        }
    }
}
