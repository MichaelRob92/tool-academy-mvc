﻿+function ($) {
	$(function () {
		$('.field-validation-valid, .field-validation-error').each(function () {
			$(this).addClass('help-block');
		});

		$('.validation-summary-errors').each(function () {
			$(this).addClass('alert alert-danger');
		});

		$('form').submit(function () {
			var $form = $(this);
			if ($form.valid()) {
				$form.find('.form-group').each(function () {
					var $formGroup = $(this);
					if ($formGroup.find('.field-validation-error').length === 0) {
						$formGroup.removeClass('has-error');
						$formGroup.addClass('has-success');
					}
				});
			} else {
				$form.find('.form-group').each(function () {
					var $formGroup = $(this);
					if ($formGroup.find('.field-validation-error').length > 0) {
						$formGroup.removeClass('has-success');
						$formGroup.addClass('has-error');
					}
				});
				$('.validation-summary-errors').each(function () {
					var $validationSummary = $(this);
					if (!$validationSummary.hasClass('alert-danger')) {
						$validationSummary.addClass('alert alert-danger');
					}
				});
			}
		});

		$('form').each(function () {
			var $form = $(this);
			$form.find('.form-group').each(function () {
				var $formGroup = $(this);
				if ($formGroup.find('.field-validation-error').length > 0) {
					$formGroup.addClass('has-error');
				}
			});
		});
	});

	$.validator.setDefaults({
	    errorElement: "span",
	    errorClass: "help-block",
	    highlight: function (element) {
	        $(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function (element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
	    errorPlacement: function (error, element) {
	        if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    }
	});
}(jQuery);