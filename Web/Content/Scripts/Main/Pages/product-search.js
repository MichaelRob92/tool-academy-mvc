﻿+(function ($) {
    $(function () {
        var errorAlert = $('<div class="alert alert-danger"><p>An unknown error has occured while searching for products, please <a href="#" class="alert-link" data-refresh>click here</a> to try again.</p></div>');
        var emptyAlert = $('<div class="alert alert-warning"><p>No products found, please try again.</p></div>');

        var searchUrl = $('#searchUrl').val();

        var productTemplateHtml = $('#productTemplate').html();
        var productTemplate = Hogan.compile(productTemplateHtml);

        var paginationTemplateHtml = $('#paginationTemplate').html();
        var paginationTemplate = Hogan.compile(paginationTemplateHtml);

        var $productLoaderContainer = $('[data-product-loader]'),
            $productSearchContainer = $('[data-product-search]'),
            $productCurrentPage = $('[data-product-current-page]'),
            $productTotalPages = $('[data-product-total-pages]'),
            $productCount = $('[data-product-count]'),
            $productPerPage = $('[data-product-per-page]'),
            $productPagination = $('[data-product-pagination]');

        var jqxhr = null;

        fetch();

        function fetch() {
            $productLoaderContainer.show();
            $productSearchContainer.empty();

            var phrase = $('[data-product-phrase]').val(),
                currentPage = parseInt($productCurrentPage.text(), 10),
                perPage = parseInt($productPerPage.val(), 10);

            if (jqxhr !== null) {
                jqxhr.abort();
                $productLoaderContainer.show();
            }

            $('li', $productPagination).addClass('disabled');

            jqxhr = $.ajax({
                url: searchUrl + '?phrase=' + phrase + '&currentPage=' + currentPage + '&perPage=' + perPage,
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (data) {
                    if (data.totalCount <= 0) {
                        emptyAlert.appendTo($productSearchContainer);
                    }

                    $.each(data.products, function (i, e) {
                        e.hasImage = typeof e.image !== 'undefined';
                        $productSearchContainer.append(productTemplate.render(e));
                    });

                    currentPage = data.currentPage;

                    $productCurrentPage.html(currentPage);
                    $productTotalPages.html(data.totalPages);
                    $productCount.html(data.totalCount);

                    if (typeof history.pushState !== 'undefined')
                        history.pushState({}, '', '/search?q=' + phrase + '&page=' + currentPage + '&count=' + perPage);

                    pagination();
                }
            }).error(function (o, textStatus) {
                if (textStatus !== 'abort') {
                    errorAlert.appendTo($productSearchContainer);
                }
            }).always(function () {
                $productLoaderContainer.hide();
                jqxhr = null;
            });

            return false;
        }

        function pagination() {
            var current = parseInt($productCurrentPage.text(), 10),
                totalPages = parseInt($productTotalPages.text(), 10);

            var previous = [],
                next = [],
                laquo = false,
                raquo = false;

            if (current > 1) {
                laquo = current - 1;
            }

            if (current < totalPages) {
                raquo = current + 1;
            }

            var pageDifference = 0;

            var page = current - 4;
            if (page < 1) {
                page = 1;
                pageDifference = -((current - 4) - 1);
            }

            while (page < current) {
                previous.push({ page: page });
                page++;
            }

            var maxPage = current + 4;
            if (maxPage > totalPages) {
                pageDifference = maxPage - totalPages;
                maxPage = totalPages;
            } else if (pageDifference > 0) {
                maxPage += pageDifference;
                if (maxPage > totalPages) {
                    maxPage = totalPages;
                }
            }

            page = current + 1;

            while (page <= maxPage) {
                next.push({ page: page });
                page++;
            }

            // Handles the offset if we're closer to the end of pagination
            if (pageDifference > 0) {
                previous = [];
                page = current - 4 - pageDifference;
                if (page < 1) {
                    page = 1;
                }
                while (page < current) {
                    previous.push({ page: page });
                    page++;
                }
            }

            var paginationObject = {
                laquo: laquo,
                previous: previous,
                current: current,
                next: next,
                raquo: raquo
            };

            $productPagination.empty().append(paginationTemplate.render(paginationObject));
        }

        $(document).on('click', '[data-product-page]', function () {
            if ($(this).parent().hasClass('disabled'))
                return false;

            var page = $(this).data('product-page');
            $productCurrentPage.html(page);
            return fetch();
        });

        $(document).on('click', '[data-refresh]', function () {
            return fetch();
        });

        $productPerPage.on('change', function () {
            $productCurrentPage.html('1');
            fetch();
        });
    });
})(jQuery);