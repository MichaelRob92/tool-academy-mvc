﻿+function ($) {
    $(function () {
        var mainFeature = $('#main-feature');
        if (mainFeature.length > 0) {
            $('.item', mainFeature).first().addClass('active');
        }
    });
}(jQuery);