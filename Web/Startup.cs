﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Total.ToolAcademy.Web.Startup))]

namespace Total.ToolAcademy.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
