﻿using System.Web;
using System.Web.Mvc;
using Castle.MicroKernel.Facilities;
using Castle.MicroKernel.Registration;
using Microsoft.Owin.Security;
using Total.ToolAcademy.Context;
using Total.ToolAcademy.Context.Identity;
using Total.ToolAcademy.Framework;

namespace Total.ToolAcademy.Web.Windsor.Facilities
{
    public class PersistenceFacility : AbstractFacility
    {
        protected override void Init()
        {
            Kernel.Register(
                Component.For<IDbContext>()
                    .ImplementedBy<ToolAcademyContext>()
                    .LifestylePerWebRequest(),
                Component.For<IActionInvoker>()
                    .ImplementedBy<WindsorAsyncActionInvoker>()
                    .LifestylePerWebRequest(),
                Component.For<ToolAcademyUserManager>()
                    .UsingFactoryMethod((kernel, context) => ToolAcademyUserManager.Create(Startup.DataProtectionProvider, kernel.Resolve<IDbContext>()))
                    .LifestylePerWebRequest(),
                Component.For<IAuthenticationManager>()
                    .UsingFactoryMethod((kernel, context) => HttpContext.Current.GetOwinContext().Authentication)
                    .LifestylePerWebRequest()
            );
        }
    }
}