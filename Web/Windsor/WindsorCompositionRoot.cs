﻿using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using Castle.MicroKernel;

namespace Total.ToolAcademy.Web.Windsor
{
    public class WindsorCompositionRoot : IHttpControllerActivator
    {
        private readonly IKernel _kernel;

        public WindsorCompositionRoot(IKernel kernel)
        {
            _kernel = kernel;
        }

        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            var controller = _kernel.Resolve(controllerType) as IHttpController;
            if (controller == null)
                return null;
            request.RegisterForDispose(new Release(() => _kernel.ReleaseComponent(controller)));
            return controller;
        }

        private class Release : IDisposable
        {
            private readonly Action _release;
            public Release(Action release)
            {
                _release = release;
            }

            public void Dispose()
            {
                _release();
            }
        }
    }
}