﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Mvc.Async;
using Castle.MicroKernel;

namespace Total.ToolAcademy.Web.Windsor
{
    public class WindsorAsyncActionInvoker : AsyncControllerActionInvoker
    {
        private readonly IKernel _kernel;

        public WindsorAsyncActionInvoker(IKernel kernel)
        {
            _kernel = kernel;
        }
        
        protected override IAsyncResult BeginInvokeActionMethodWithFilters(ControllerContext controllerContext,
            IList<IActionFilter> filters, ActionDescriptor actionDescriptor, IDictionary<string, object> parameters,
            AsyncCallback callback, object state)
        {
            InjectProperties(filters);
            return base.BeginInvokeActionMethodWithFilters(controllerContext, filters, actionDescriptor, parameters, callback, state);
        }

        protected override ActionExecutedContext InvokeActionMethodWithFilters(ControllerContext controllerContext,
            IList<IActionFilter> filters, ActionDescriptor actionDescriptor, IDictionary<string, object> parameters)
        {
            InjectProperties(filters);
            return base.InvokeActionMethodWithFilters(controllerContext, filters, actionDescriptor, parameters);
        }

        private void InjectProperties(IEnumerable<IActionFilter> filters)
        {
            foreach (var filter in filters)
                _kernel.InjectProperties(filter);
        }
    }

    public static class WindsorExtensions
    {
        public static void InjectProperties(this IKernel kernel, object target)
        {
            var type = target.GetType();
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var property in properties)
            {
                if (!property.CanWrite || !kernel.HasComponent(property.PropertyType))
                    continue;

                var value = kernel.Resolve(property.PropertyType);
                try
                {
                    property.SetValue(target, value);
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Error setting property {0} on type {1}.", property.Name, type.FullName), ex);
                }
            }
        }
    }
}