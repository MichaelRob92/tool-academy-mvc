﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Total.ToolAcademy.Web.ViewModels
{
    public class SearchViewModel
    {
        public string Phrase { get; set; }
        public int Page { get; set; }
        public int Count { get; set; }
        public IEnumerable<int> ValidCounts { get; set; } 
    }

    public class SearchResultsViewModel
    {
        public int TotalCount { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public IEnumerable<ProductSearchViewModel> Products { get; set; }
    }

    public class ProductSearchViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Summary { get; set; }
        public string Content { get; set; }
        public string DisplayPrice { get; set; }
        [JsonIgnore]
        public decimal Price { get; set; }
        [JsonIgnore]
        public decimal? OfferPrice { get; set; }
        [JsonIgnore]
        public decimal ActualPrice { get; set; }
        [JsonIgnore]
        public bool VatExempt { get; set; }
        [JsonIgnore]
        public decimal? VatRate { get; set; }
        public string Image { get; set; }
        public string ImageAlt { get; set; }
        public decimal Weight { get; set; }
        public string Permalink { get; set; }
    }

}