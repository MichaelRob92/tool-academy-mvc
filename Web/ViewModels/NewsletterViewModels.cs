﻿using System.ComponentModel.DataAnnotations;

namespace Total.ToolAcademy.Web.ViewModels
{
    public class SignupFormViewModel
    {
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter a valid email address")]
        [StringLength(256, ErrorMessage = "Please enter a valid email address")]
        [EmailAddress(ErrorMessage = "Please enter a valid email address")]
        [Display(Name = "Email Address")]
        public string Address { get; set; }
    }
}