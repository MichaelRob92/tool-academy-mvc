﻿using System.Collections.Generic;

namespace Total.ToolAcademy.Web.ViewModels
{
    public class MainNavigationViewModel
    {
        public IEnumerable<NavigationMenuViewModel> Menus { get; set; }
    }

    public class NavigationMenuViewModel
    {
        public string Name { get; set; }

        public string Link { get; set; }

        public IEnumerable<NavigationMenuLinkViewModel> Links { get; set; }
    }

    public class NavigationMenuLinkViewModel
    {
        public string Name { get; set; }

        public string Link { get; set; }
    }
}