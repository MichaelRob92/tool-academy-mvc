﻿namespace Total.ToolAcademy.Web.ViewModels
{
    public class BasketOverviewViewModel
    {
        public int Count { get; set; }

        public decimal Total { get; set; }
    }
}