﻿using System.Collections.Generic;
using Total.ToolAcademy.Web.Areas.Brands.ViewModels;

namespace Total.ToolAcademy.Web.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<BannerViewModel> Banners { get; set; }

        public IEnumerable<BrandViewModel> Brands { get; set; } 
    }

    public class BannerViewModel
    {
        public string Image { get; set; }
        public string ImageAlt { get; set; }
    }
}