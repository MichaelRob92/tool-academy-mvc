﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Total.ToolAcademy.Entity;
using Total.ToolAcademy.Framework;
using Total.ToolAcademy.Helpers;
using Total.ToolAcademy.Web.ViewModels;

namespace Total.ToolAcademy.Web.Controllers
{
    [RoutePrefix("api/products")]
    public class ProductsApiController : ApiController
    {
        private readonly IDbContext _context;

        public ProductsApiController(IDbContext context)
        {
            _context = context;
        }

        private readonly List<int> _validCounts = new List<int>
        {
            8, 16, 32, 64
        }; 

        [Route("search")]
        [HttpGet]
        public async Task<SearchResultsViewModel> Search(string phrase, int currentPage, int perPage)
        {
            if (string.IsNullOrWhiteSpace(phrase))
                return null;

            phrase = phrase.ToLower();
            if (!_validCounts.Contains(perPage))
                perPage = 8;
            if (currentPage < 1)
                currentPage = 1;

            var currentDate = DateTime.Now;
            var terms = phrase.Split(' ');

            var productCount = await _context.Products
                .Where(e => !e.Deleted)
                .Where(e=> e.Status == ProductStatus.Live || (e.Status == ProductStatus.TimeLimited && e.StartDate <= currentDate && e.FinishDate >= currentDate))
                .Where(e => terms.All(t => e.Code.ToLower().Contains(t) || e.Name.ToLower().Contains(t) || e.Summary.ToLower().Contains(t) || e.Content.ToLower().Contains(t)))
                .CountAsync();

            var pageCount = (int)Math.Ceiling((double)productCount / perPage);
            if (pageCount < currentPage)
                currentPage = 1;

            var products = await _context.Products
                .Where(e => !e.Deleted)
                .Where(e=> e.Status == ProductStatus.Live || (e.Status == ProductStatus.TimeLimited && e.StartDate <= currentDate && e.FinishDate >= currentDate))
                .Where(e => terms.All(t => e.Code.ToLower().Contains(t) || e.Name.ToLower().Contains(t) || e.Summary.ToLower().Contains(t) || e.Content.ToLower().Contains(t)))
                .OrderBy(e => e.SortOrder)
                .Select(e => new ProductSearchViewModel
                {
                    Id = e.Id,
                    Code = e.Code,
                    Name = e.Name,
                    Price = e.Price,
                    OfferPrice = e.OfferPrice,
                    VatExempt = e.VatExempt,
                    VatRate = e.VatRate,
                    Image = e.Image,
                    ImageAlt = e.ImageAlt,
                    Permalink = e.Permalink,
                    ActualPrice = e.OfferPrice ?? e.Price
                })
                .Skip((currentPage * perPage) - perPage)
                .Take(perPage)
                .ToListAsync();

            products.ForEach(a => a.DisplayPrice = a.ActualPrice.CalculateVatPrice(a.VatExempt, a.VatRate).ToString("N2"));

            var searchViewModel = new SearchResultsViewModel
            {
                Products = products,
                TotalCount = productCount,
                CurrentPage = currentPage,
                TotalPages = pageCount
            };

            return searchViewModel;
        }
    }
}
