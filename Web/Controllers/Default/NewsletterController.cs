﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Mvc;
using Castle.Core.Logging;
using Total.ToolAcademy.Entity;
using Total.ToolAcademy.Framework;
using Total.ToolAcademy.Helpers;
using Total.ToolAcademy.Web.ViewModels;

namespace Total.ToolAcademy.Web.Controllers
{
    public class NewsletterController : Controller
    {
        public ILogger Logger { get; set; }

        private readonly IDbContext _context;

        public NewsletterController(IDbContext context)
        {
            _context = context;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Signup(SignupFormViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var address = model.Address.ToLower();
            if (!address.IsValidEmail())
            {
                ModelState.AddModelError("Address", "Please enter a valid email address.");
                return View(model);
            }

            var exists = await _context.MailingLists.AnyAsync(e => e.EmailAddress == address);
            if (exists)
            {
                ModelState.AddModelError(string.Empty, "Thank you, but you have already signed up for our newsletter!");
                return View(model);
            }

            _context.MailingLists.Add(new MailingList { EmailAddress = address });
            await _context.SaveChangesAsync();

            Logger.InfoFormat("Newsletter signup: {0}", address);

            return Redirect("~/thank-you-newsletter");
        }
    }
}