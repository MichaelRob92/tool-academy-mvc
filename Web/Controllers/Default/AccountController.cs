﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Total.ToolAcademy.Context.Identity;
using Total.ToolAcademy.Entity;
using Total.ToolAcademy.Web.ViewModels;

namespace Total.ToolAcademy.Web.Controllers
{
    [RoutePrefix("account")]
    [Authorize]
    public class AccountController : Controller
    {
        private ToolAcademyUserManager _userManager;

        public ToolAcademyUserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ToolAcademyUserManager>(); }
            private set { _userManager = value; }
        }

        [Route("")]
        // GET: /account/
        public ActionResult Index()
        {
            return View();
        }

        [Route("~/login")]
        [AllowAnonymous]
        public ActionResult Login(string @ref)
        {
            ViewBag.ReferralUrl = @ref;
            return View();
        }

        [Route("~/login")]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string @ref)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = await UserManager.FindAsync(model.EmailAddress, model.Password);
            if (user == null)
            {
                ModelState.AddModelError(string.Empty, "Invalid username or password.");
                return View(model);
            }

            var customer = await UserManager.IsInRoleAsync(user.Id, "Customer");
            if (!customer)
            {
                ModelState.AddModelError(string.Empty, "Invalid username or password.");
                return View(model);
            }

            await SignInAsync(user, model.RememberMe);
            return RedirectToLocal(@ref);
        }

        [Route("~/register")]
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [Route("~/register")]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = new User
            {
                UserName = model.EmailAddress,
                Email = model.EmailAddress
            };
            var result = await UserManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                await UserManager.AddToRoleAsync(user.Id, "Customer");
                await SignInAsync(user, false);
                return RedirectToAction("Index", "Home");
            }
            AddErrors(result);
            return View(model);
        }

        [Route("forgot-password")]
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [Route("forgot-password")]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (Request.Url == null)
            {
                ModelState.AddModelError(string.Empty, "Invalid request url.");
                return View(model);
            }

            if (!ModelState.IsValid)
                return View(model);

            var user = await UserManager.FindByNameAsync(model.EmailAddress);
            if (user == null)
            {
                ModelState.AddModelError(string.Empty, "We could not find a user account associated to that email address.");
                return View(model);
            }

            var customer = await UserManager.IsInRoleAsync(user.Id, "Customer");
            if (!customer)
            {
                ModelState.AddModelError(string.Empty, "We could not find a user account associated to that email address.");
                return View(model);
            }

            var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            var callbackUrl = Url.Action("ResetPassword", "Account", new {userId = user.Id, code}, Request.Url.Scheme);
            await UserManager.SendEmailAsync(user.Id, "Reset Password", string.Format("Please reset your password by clicking <a href=\"{0}\">here</a>", callbackUrl));
            return RedirectToAction("ForgotPasswordConfirmation", "Account");
        }

        [Route("forgot-password-confirmation")]
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [Route("reset-password")]
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("GenericError") : View();
        }

        [Route("reset-password")]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = await UserManager.FindByNameAsync(model.EmailAddress);
            if (user == null)
            {
                ModelState.AddModelError(string.Empty, "No user found");
                return View(model);
            }

            var customer = await UserManager.IsInRoleAsync(user.Id, "Customer");
            if (!customer)
            {
                ModelState.AddModelError(string.Empty, "We could not find a user account associated to that email address.");
                return View(model);
            }

            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
                return RedirectToAction("ResetPasswordConfirmation", "Account");

            AddErrors(result);
            return View(model);
        }

        [Route("reset-password-confirmation")]
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        [Route("sign-out")]
        [HttpPost]
        public ActionResult SignOut()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        #region Helpers

        private IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        private async Task SignInAsync(User user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = isPersistent
            }, await user.GenerateUserIdentityAsync(UserManager));
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);
            return RedirectToAction("Index", "Home");
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
                ModelState.AddModelError(string.Empty, error);
        }

        #endregion
    }
}