﻿using System.Web.Mvc;

namespace Total.ToolAcademy.Web.Controllers
{
    public class StatusController : Controller
    {
        // GET: /404
        public ActionResult NotFound()
        {
            return View();
        }
    }
}