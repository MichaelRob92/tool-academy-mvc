﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Total.ToolAcademy.Entity;
using Total.ToolAcademy.Framework;
using Total.ToolAcademy.Web.Areas.Brands.ViewModels;
using Total.ToolAcademy.Web.ViewModels;

namespace Total.ToolAcademy.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDbContext _context;

        private const int BannerCount = 5;
        private const int BrandCount = 6;

        public HomeController(IDbContext context)
        {
            _context = context;
        }

        //[OutputCache(Duration = 900)]
        // GET: /
        public async Task<ActionResult> Index()
        {
            var banners = await _context.Banners
                .Where(e => e.Position == BannerPosition.MainFeature)
                .OrderBy(e => e.SortOrder)
                .Take(BannerCount)
                .Select(b => new BannerViewModel
                {
                    Image = b.Image,
                    ImageAlt = b.ImageAlt
                })
                .ToListAsync();

            var currentDate = DateTime.Now;
            var brands = await _context.Brands
                .Where(e => !e.Deleted && (e.Status == BrandStatus.Live || (e.Status == BrandStatus.TimeLimited && e.StartDate <= currentDate && e.FinishDate >= currentDate)))
                .OrderBy(e => e.SortOrder)
                .Take(BrandCount)
                .Select(b => new BrandViewModel
                {
                    Name = b.Name, 
                    Image = b.Image, 
                    ImageAlt = b.ImageAlt,
                    Permalink = b.Permalink
                })
                .ToListAsync();

            return View(new HomeViewModel
            {
                Banners = banners,
                Brands = brands
            });
        }
    }
}