﻿using System.Collections.Generic;
using System.Web.Mvc;
using Total.ToolAcademy.Web.ViewModels;

namespace Total.ToolAcademy.Web.Controllers
{
    public class SearchController : Controller
    {
        private readonly List<int> _validCounts = new List<int>
        {
            8, 16, 32, 64
        }; 

        //[OutputCache(Duration = 900, VaryByParam = "q;page;count")]
        // GET: /search/?q={phrase}
        public ActionResult Index(string q, int page = 1, int count = 8)
        {
            var phrase = q.ToLower();
            ViewBag.SearchPhrase = phrase;

            if (!_validCounts.Contains(count))
                count = 8;
            
            return View(new SearchViewModel
            {
                Phrase = phrase,
                Page = page,
                Count = count,
                ValidCounts = _validCounts
            });
        }
    }
}